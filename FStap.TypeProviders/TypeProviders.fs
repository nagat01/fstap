module FStap.TypeProvidersInternal
open System open System.Windows open System.Windows.Controls
open System.Windows.Media.Imaging
open System.IO
open System.Reflection
open Microsoft.FSharp.Core.CompilerServices
open Microsoft.FSharp.Quotations
open ProviderImplementation.ProvidedTypes 
[<TypeProviderAssembly>] ()

// util
let If b t f = if b then t else f

// filer
let (+/) a b = Path.Combine(a, b)

type String with
  member __.files pat = Directory.GetFiles(__, If (pat = "") "*.*" pat)
  member __.nameNoExt = Path.GetFileNameWithoutExtension __


// image
let mkBitmapSource (bs:byte[]) = 
  use stream = new IO.MemoryStream(bs)
  let dec = PngBitmapDecoder(stream, BitmapCreateOptions.PreservePixelFormat, BitmapCacheOption.OnLoad)
  dec.Frames.[0]

let mkImage w (bs:byte[]) = Image (Source=mkBitmapSource bs, Width=w)


// type provider
let asm = Assembly.GetExecutingAssembly() 
let ns = "FStap"

let ptype name = 
  ProvidedTypeDefinition(asm, ns, name, Some typeof<obj>)

let pctor<'__> = 
  ProvidedConstructor(
    [ProvidedParameter("width", typeof<float>)], 
    InvokeCode = fun __-> <@@ (%%__.[0]:float):>obj @@> )

[<TypeProvider>]
type IcoTypeProvider(config:TypeProviderConfig) as __ =
  inherit TypeProviderForNamespaces()                           
  let img    = ptype "ImageProvider"
  let bmpSrc = ptype "BitmapSourceProvider"
  do
    img.AddMember pctor
    for file in (config.ResolutionFolder +/ "res").files "" do
      let bs = File.ReadAllBytes file
      let name = file.nameNoExt
      ProvidedProperty(
        name, 
        typeof<Image>, 
        GetterCode = fun __-> <@@ mkImage ((%%__.[0]:obj) :?> float) bs @@> )
      |> img.AddMember
      ProvidedProperty(
        name, 
        typeof<BitmapFrame>, 
        IsStatic=true, 
        GetterCode=fun __-> <@@ mkBitmapSource bs @@> )
      |> bmpSrc.AddMember
    __.AddNamespace(ns, [img; bmpSrc])