﻿[<AutoOpen>]
module FStap.String
open System
open System.Text.RegularExpressions
open Printf

let inline sf      fmt = sprintf fmt
let inline sfn     fmt = ksprintf (_1 + "\r\n") fmt
let inline kf cont fmt = kprintf cont fmt

let join sep ss = String.concat sep    ss


type String with
  member __.good = String.IsNullOrWhiteSpace(__).n
  member __.hasLower (s:s) = __.lower.Contains s.lower
  member __.isMat pat  = Regex.IsMatch(__, pat) 
  member __.mat   pat  = Regex.Match  (__, pat)
  member __.occ   pat  = Regex.Matches(__, pat).Count
  member __.urlOk = Uri.IsWellFormedUriString(__, UriKind.Absolute)

  member __.Or s  = If __.good __ s
  member __.map f = String.map f __

  member __.i     = i.TryParse   __ |> tupleToSome
  member __.i64   = i64.TryParse __ |> tupleToSome
  member __.f     = f.TryParse   __ |> tupleToSome
  member __.b     = b.TryParse   __ |> tupleToSome

  member __.lower:s = __.ToLower()
  member __.upper:s = __.ToUpper()

  member __.repRegex  pat rep = Regex.Replace(__, pat, (rep:s))
  member __.remRegex  pat     = __.repRegex pat ""
  member __.repsRegex reps = Seq.fold (fun (inp:s) (pat, rep)-> inp.repRegex pat rep) __ reps
  member __.remsRegex rems = Seq.fold(fun (__:s) rem -> __.rep rem "") __ rems
  member __.rep pat rep = __.Replace(pat, (rep:s))

  member __.trim  = __.Trim()
  member __.kvs   = __.split [";"; "\r\n"] >%?? (fun (kv:s) ->
    kv.split "," |> function [| k; v|] -> Some (k, v) | _ -> None)
  member __.lines = __.Split([|"\r\n"|], StringSplitOptions.RemoveEmptyEntries)
  member __.words = __.Split()
  member __.split (ss:s seq) = __.Split(ss.array, StringSplitOptions.RemoveEmptyEntries)
  member __.split (s:s) = __.split [s]
  member __.ss    = seq { for c in __ -> c.s }
  member __.chunk n = seq { 
    for i in 0 .. __.len / n - 1 -> 
      __.Substring(i * n, n) }

  member __.isEnglish = 
    let isAscii (__:c) = 0 <= __.i && __.i <= 0x7f
    (__ >%? isAscii).len.f / __.len.f > 0.8


type Match with
  member __.group n = __.Groups.[n:i].Value

type[<Ex>]StrEx () = 
 [<Ex>]static member concat (ss, sep) = String.concat sep ss    
