﻿[<AutoOpen>]
module FStap.Builders
open System open System.Windows open System.Windows.Controls
open System.Threading.Tasks


let using   (v, f) = let res = f v in (v :> IDisposable).Dispose(); res
let tryWith (m, h) = try m () with e -> h e


type LazyBuilder () =
  member __.Using vf      = using vf
  member __.Delay f       = f
  member __.Zero  x       = x
  member __.Yield x       = x
  member __.TryWith(m, h) = tryWith (m, h)
  member __.Combine(m, n) = m; n ()
  member __.Bind(m, f)    = if m then f ()
  member __.Bind(m, f)    = m (fun v -> f v)
  member __.Bind (m:_ seq,  f) = m |%| f
  member __.Bind (m:_ opt,  f) = m |%| f
  member __.Bind (m:_ opt', f) = m |%| f
  member __.Bind (m:_ a,    f) = m >>= f
  member __.For (xs, f)  = for x in xs do f x
  member __.For(xsm:_ opt', f) = xsm.iter (Seq.iter f) 
  member __.While (c, f) = while c () do f ()
  [<CustomOperation "V">]
  member __.v   (_, x) = x
  [<CustomOperation "is">]
  member __.is  (_, x) = x
  [<CustomOperation "Not">]
  member __.Not (_, x) = not x


type FuncBuilder<'a,'b,'c> (run: ('a->'b)->'c) =
  inherit LazyBuilder()
  member __.Run f = run f


let try'        = FuncBuilder(fun f -> try f () with __ -> ())
let tryb<'a>    = FuncBuilder(fun f -> try f () with __ -> false)
let tryd<'a>    = FuncBuilder(fun f -> try f () with __ -> Zero<'a>)
let trym<'a>  s = FuncBuilder(fun f -> try f () with __ -> MessageBox.Show(sprintf "message = %s\r\nstacktrace=\r\n%s\r\n\r\n%s" __.Message __.StackTrace s).ig)

type Try_BaseBuilder() =
  member __.Using mf     = using mf
  member __.Delay f      = f
  member __.Return x     = Some x
  member __.ReturnFrom x = x
  member __.Yield      x = Some x
  member __.YieldFrom  x = x
  [<CustomOperation "V">]
  member __.v   (_, x)  = __.Yield x


type Try_Builder (catch) =
  inherit Try_BaseBuilder ()
  member __.Run f = try f () with __ -> catch __; None

let try_    = Try_Builder ig


type OptBuilder() =
  member __.Return v       = Some v
  member __.ReturnFrom v   = v
  member __.Delay f        = f ()    
  member __.Using vf       = using vf
  member __.Bind (b,f)     = if b then f () else None
  member __.Bind (m, f)    = bind f (m:_ opt)
  member __.Combine((), m) = m
  member __.Combine(m, n)  = bind (fun _ -> n) (m:_ opt)

let opt = OptBuilder ()

type IOptBuilder() =                 
  member __.Zero x = x
  member __.Run  m = ()
  member __.Bind (m:_ opt', f) = m.iter f
  member __.Bind (m:_ opt,  f) = Option.iter f m
  member __.Bind (m, f) = if m <> null then f m
  member __.Combine((), n) = n 
  member __.Delay x = x ()

let iopt = IOptBuilder()


type AsyncBaseBuilder() =
  member __.Return x     = async { return x }
  member __.ReturnFrom m = m
  member __.Yield x      = x
  member __.Zero ()      = async { () }
  member __.Delay m      = m

  member __.Combine (m:_ a, n) = m >>= n
  member __.Using (v,       f) = async { 
    let! res = f v 
    (v :> IDisposable).Dispose()
    return res }
  member __.Bind (m:i,      f) = async { 
    do! Async.Sleep m 
    return! f () }
  member __.Bind (m:f,      f) = __.Bind(int(m*1000.), f)
  member __.Bind (m:TimeSpan,f)= __.Bind(m.TotalSeconds, f)
  member __.Bind (m:b,      f) = async { if m    then return! f () }
  member __.Bind (m:_ opt,  f) = async { if m.ok then return! f m.v }
  member __.Bind (m:_ opt', f) = async { if m.ok then return! f m.v }
  member __.Bind (m:_ obs,  f) = bind f (awaitObs m)
  member __.Bind (m:_ Event,f) = bind f (awaitObs m.Publish)
  member __.Bind (m:_ Task, f) = bind f (Async.AwaitTask m)
  member __.Bind (m:  Task, f) = bind f (m |> Async.AwaitIAsyncResult |> Async.Ignore)
  member __.Bind (m:_ a,    f) = bind f m
  member __.For  (xs:#seq<_>, f) = for x in xs do f x
  member __.For  (xs:#seq<_>, f) = async { for x in xs do do! f x }
  member __.For  (xsm,      f) = async { let! xs = xsm:_ seq a in do! Seq.iterAsync f xs }
  member __.For  (xsm,      f) = async { let! xs = xsm:_ seq a in return xs.collect f }
  member __.For  (xsom,     f) = async { let! xso = xsom:_ seq opt a in do! __.Bind(xso, fun xs -> Seq.iterAsync f xs)}
  member __.For  (xsom,     f) = async { let! xso = xsom:_ seq opt a in return (defaultArg xso Seq.empty).collect f }
  member __.While (b,       f) = async { while b () do do! f () }


type AsyncBuilder() =
  inherit AsyncBaseBuilder()
  member __.Run f = async { return! f () } 

let async = AsyncBuilder()


type AsyncStartBuilder<'a> (start: 'a a-> 'a, run: (u->'a a) -> 'a a) =
  inherit AsyncBaseBuilder()
  member __.Run f = async { return! run f } |> start

let immediate'       = AsyncStartBuilder (immediate, fun f -> f ())
let tryImmediate'    = AsyncStartBuilder (immediate, fun f -> try f () with _ -> async { () })

let immediateLoop' (n:i) = 
  AsyncStartBuilder (immediate, fun f -> async {
    while true do
      do! n
      do! f () } )

let imRetry count (interval:i) cond = 
  AsyncStartBuilder (immediate, fun f -> async {
    let mutable n    = count
    let mutable next = true
    while (&n -= 1; n > 0) && next do
      do! interval
      if cond () then
        next <- false
        do! f () } )


type TaskBuilder() =
  inherit LazyBuilder()
  member __.Run f = async { return f () } |> Async.StartAsTask |> Async.AwaitTask

let task' = TaskBuilder()


type Int32 with
  member __.times = FuncBuilder(fun f -> for _ in 1 .. __ do f ())


type 'a ImmediateHandlerBuilder (m: 'a obs, run) =
  inherit AsyncBaseBuilder()
  member __.Run f = m => (f >> run)

let inline (=+!)  m = ImmediateHandlerBuilder (toObs m, immediate)


type HandlerBuilder<'a,'b> (run:'a->'b) =
  inherit LazyBuilder()
  member __.Run f = run f

let inline (=+)  m = HandlerBuilder (fun f -> m => f)
let inline (=+?) m = HandlerBuilder (fun f -> m => fun x -> try' { f x })


type EventMapBuilder<'a,'b> (m:'a obs) =
  inherit LazyBuilder()
  member __.Run f = m |%> f

let inline (-%) m = EventMapBuilder (toObs m)

type 'a EventFilterBuilder (m: 'a obs) =
  inherit LazyBuilder()
  member __.Run f = Observable.filter f m
  member __.Run f = Observable.choose f m

let inline (-?) m = EventFilterBuilder (toObs m)


type DoBuilder () =
  inherit LazyBuilder()
  member __.Run f = f ()

let do' = DoBuilder()


let trace<'a> isMessageBox = FuncBuilder(fun f ->
  if not isMessageBox then
    f ()
  else
    try 
      f () 
    with e -> 
      let mutable e = e
      while e.InnerException.ok do 
        e <- e.InnerException
      let text = 
        String.concat "\r\n" [
          "message = " + e.Message 
          ""
          "target site = " + if e.TargetSite.ok then e.TargetSite.Name else ""
          ""
          "stacktrace = " + if e.StackTrace.ok then e.StackTrace else "" ] 
      let tbx = TextBox (Text=text)
      let w   = Window(Title= "エラーメッセージです。ごめんなさい。", Content=tbx)
      w.ShowDialog().ig
      Def<'a> )

