﻿[<AutoOpen>]
module FStap.Abbrev
open System open System.Windows
open System.Reflection
open System.Runtime.InteropServices
open System.Runtime.CompilerServices


type Dll = DllImportAttribute
type Ex  = ExtensionAttribute           


type product     = AssemblyProductAttribute     
type title       = AssemblyTitleAttribute
type description = AssemblyDescriptionAttribute
type company     = AssemblyCompanyAttribute
type copyright   = AssemblyCopyrightAttribute
type version     = AssemblyVersionAttribute


type o   = obj
type u   = unit
type b   = bool
type c   = char
type s   = String
type i   = int
type i64 = int64
type u32 = uint32
type u64 = uint64
type y   = byte
type f   = float
type ni  = IntPtr

type 'a opt = 'a option
type 'a a   = 'a Async 
type 'a obs = 'a IObservable
type 'a ra  = 'a ResizeArray

type 'a Queue = 'a System.Collections.Generic.Queue

type Dictionary<'a,'b when 'a: equality> = System.Collections.Generic.Dictionary<'a,'b>

type Stopwatch = Diagnostics.Stopwatch

type Key = Input.Key
type Po  = Point
type Vec = Vector
type Sz  = Size
type R32 = Int32Rect
