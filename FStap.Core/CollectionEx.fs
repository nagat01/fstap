﻿[<AutoOpen>]
module FStap.CollectionEx
open System.Collections.Generic
open Microsoft.FSharp.Collections


let nC2 xs =
  let rec f xs =
    match xs with
    | x :: tail -> [ for y in tail -> x, y ] @ f tail
    | _ -> []
  f xs

let cart2 m n = seq { for y in m do for x in n -> y,x }


type [<Ex>] CollectionEx () = 
 [<Ex>]static member inline map     (__, f) = Seq.map f __
 [<Ex>]static member inline find    (__, f) = Option.map snd (List.tryFind (fst >> f) __)
 [<Ex>]static member inline indexOf (__, x) = Seq.findIndex ((=)x) __
 [<Ex>]static member inline have    (__, x) = Seq.exists ((=)x) __
 [<Ex>]static member inline maxOf   (__, f) = Seq.map f __ |> Seq.max
 [<Ex>]static member inline sumBy   (__, f) = Seq.sumBy f __


type 'a IEnumerable with            
  member __.ok     = __ <> null && (Seq.isEmpty __).n
  member __.len    = Seq.length  __
  member __.ilast  = max 0 (__.len - 1)
  member __.head   = Seq.head    __
  member __.last   = Seq.last    __
  member __.list   = List.ofSeq  __
  member __.array  = Array.ofSeq __
  member __.rev    = List.rev __.list
  member __.seqi   = Seq.zip (Seq.initInfinite id) __

  member __.collect   f = Seq.collect   f __

  member __.filter  f = Seq.filter  f __
  member __.exists  f = Seq.exists  f __ 
  member __.forall  f = Seq.forall  f __
  member __.any     f = Seq.forall (f>>not) __ |> not

  member __.tryFind f = Seq.tryFind f __
  member __.idx     f = Seq.findIndex f __


type 'a List with
  member __.rev       = List.rev __
  member __.filter  f = filter f __


let arrayZero<'a> n  = Array.zeroCreate n : 'a[]


type 'a ``[]`` with
  member __.rand      = __.[rand.Next __.Length]
  member __.rev       = Array.rev __
  member __.shuffle   = Array.sortBy (fun _ -> rand.Next()) __
  member __.choose  f = choose f  __
  member __.filter  f = filter f  __
  member __.clear = for i in 0 .. __.ilast do __.[i] <- Zero


type IDictionary<'k,'v> with
  member __.find k = __.TryGetValue k |> tupleToSome
  member __.kv = seq { for kv in __ -> kv.Key , kv.Value }


type 'a Queue with
  member __.deq    = __.Dequeue()
  member __.tryDeq = if __.Count > 0 then Some __.deq else None
  member __.enq v  = __.Enqueue v
  member __.truncate n = while __.len > n do __.deq.ig
  member __.enqTruncate v n = __.enq v; __.truncate n
  
