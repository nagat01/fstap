﻿[<AutoOpen>]
module FStap.TypeClass
open System.Windows.Controls


type BindO() =
  static member (?<-) (_:BindO, m, f) = Option.bind   f m
  static member (?<-) (_:BindO, m, f) = Seq.collect   f m
  static member (?<-) (_:BindO, m, f) = List.collect  f m
  static member (?<-) (_:BindO, m, f) = Array.collect f m
  static member (?<-) (_:BindO, m, f) = Async.bind    f m
  static member (?<-) (_:BindO, m, x) = bToOpt m x
let inline bind    f m = BindO() ? (m) <- f
let inline (>>=)   m f = bind f m


type MapO() =
  static member (?<-) (_:MapO, m, f) = f (m:_ ref).Value
  static member (?<-) (_:MapO, m, f) = Option.map     f m
  static member (?<-) (_:MapO, m, f) = Option.map     f (m:_ opt').v' 
  static member (?<-) (_:MapO, m, f) = Seq.map        f m
  static member (?<-) (_:MapO, m, f) = List.map       f m
  static member (?<-) (_:MapO, m, f) = Array.map      f m
  static member (?<-) (_:MapO, m, f) = Observable.map f m
  static member (?<-) (_:MapO, m, f) = Event.map      f m
  static member (?<-) (_:MapO, m, f) = Async.map      f m
  static member (?<-) (_:MapO, m, f) = Async.map      f (Async.AwaitTask m)  
  static member (?<-) (_:MapO, m, n) = (for __ in m do (n:Panel).Children.Add __ |> ig); m
  static member (?<-) (_:MapO, m, n) = (for __ in m do (n:ItemsControl).Items.Add __       |> ig); m
let inline map      f m = MapO() ? (m) <- f
let inline (|%>)    m f = map f m


type IterO() =
  static member (?<-) (_:IterO, m, f) = f (m:_ ref).Value
  static member (?<-) (_:IterO, m, f) = Option.iter f m
  static member (?<-) (_:IterO, m, f) = (m:_ opt').iter f
  static member (?<-) (_:IterO, m, f) = Seq.iter    f m
  static member (?<-) (_:IterO, m, f) = List.iter   f m
  static member (?<-) (_:IterO, m, f) = Array.iter  f m
  static member (?<-) (_:IterO, m, f) = async { let! x = m in f x }
  static member (?<-) (_:IterO, m, f) = for c in (m:#Panel).Children do f c
  static member (?<-) (_:IterO, m, f) = for i in (m:#ItemsControl).Items do f i
let inline iter     f m = IterO() ? (m) <- f
let inline (|%|)    m f = iter f m


type FilterO() =
  static member (?<-) (_:FilterO, m, f) = Option.bind (fun x -> bToOpt (f x) x) m
  static member (?<-) (_:FilterO, m, f) = Seq.filter        f m
  static member (?<-) (_:FilterO, m, f) = Seq.filteri       f m
  static member (?<-) (_:FilterO, m, f) = List.filter       f m
  static member (?<-) (_:FilterO, m, f) = Array.filter      f m
  static member (?<-) (_:FilterO, m, f) = Observable.filter f m
  static member (?<-) (_:FilterO, m, f) = Event.filter      f m
let inline filter  f m = FilterO() ? (m) <- f
let inline (>%?)   m f = filter f m
let inline (>%?~)  m f = filter (f >> not) m


type ChooseO() =
  static member (?<-) (_:ChooseO, m, f) = Seq.choose        f m
  static member (?<-) (_:ChooseO, m, f) = List.choose       f m
  static member (?<-) (_:ChooseO, m, f) = Array.choose      f m
  static member (?<-) (_:ChooseO, m, f) = Observable.choose f m
  static member (?<-) (_:ChooseO, m, f) = Event.choose      f m
let inline choose  f m = ChooseO() ? (m) <- f
let inline (>%??)  m f = choose f m
let inline extract __  = __ >%?? (box >> function :? 'a as x -> Some x | _ -> None)


type MPlus() =      
  static member (?<-) (_:MPlus, m, n) = (m:i) + (n:i)                          
  static member (?<-) (_:MPlus, m, n) = (m:f) + (n:f)                          
  static member (?<-) (_:MPlus, m, n) = (m:s) + (n:s)                          
  static member (?<-) (_:MPlus, m, n) = (m:c) + (n:c)
  static member (?<-) (_:MPlus, m, n) = Observable.merge m n
  static member (?<-) (_:MPlus, m, n) = Array.append     m n
  static member (?<-) (_:MPlus, m, n) = List.append      m n
  static member (?<-) (_:MPlus, m, n) = async { 
    do! m
    return! n }
let inline (++)  m n = MPlus() ? (m) <- n


type MPlus'() =
  static member (?<-) (_:MPlus', x, m) = Seq.append (Seq.singleton x) m
  static member (?<-) (_:MPlus', m, x) = Seq.append m (Seq.singleton x)
  static member (?<-) (_:MPlus', x, m) = Array.append [|x|] m
  static member (?<-) (_:MPlus', m, x) = Array.append m [|x|]
let inline (+++) m n = MPlus'() ? (m) <- n

