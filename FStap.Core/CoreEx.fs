﻿[<AutoOpen>]
module FStap.CoreEx
open System


module Seq =
  let iterAsync f __ = async { for x in __ do do! f x }
  let choosei   f __ = __ |> Seq.mapi f |> Seq.choose id 
  let filteri   f __ = __ |> choosei (fun i x -> bToOpt(f i x) x)


module Async =
  let bind f m = async { let! x = m in return! f x }
  let map  f m = async { let! x = m in return  f x }


module Observable =
  let concat (os:#obs<'a> seq) = 
    os |> Seq.map (fun o -> o :> obs<'a>) 
    |> Seq.reduce Observable.merge
  let changed (os:#obs<'a>) =
    os |> Observable.pairwise 
    |> Observable.choose(fun(v,w) -> bToOpt (v <> w) w)


type Object with              
  member __.s   = string __
  member __.ig  = ()
  member __.ok  = __ <> null
  member __.bad = __ =  null


type Boolean with
  member __.n      = not __                                    
  member __.some v = bToOpt __ v
  member __.If f g =  If __ f g


type 'a Option with
  member __.v   = __.Value
  member __.ok  = __.IsSome
  member __.bad = __.IsNone


type Nullable< 'a when 'a: struct and 'a: (new: u->'a) and 'a:> ValueType > with 
  member __.v  = __.Value
  member __.ok = __.HasValue && match box __.Value with :? bool as b -> b | _ -> true


type 'a IObservable with
  member __.wait = Observable.map ig __


type 'a sh with
  member __.Item f = __.fire (f __.v)
  member __.ini    = __.force __.v


type[<Ex>]ExtsEx = 
 [<Ex>]static member inline S (__, fmt)= (^__:(member ToString: s->s) __, fmt)
 [<Ex>]static member inline n (__, x ) = __ x |> not
 [<Ex>]static member inline is(__, f ) = match __ with Some  __ -> f __ | _ -> false
 [<Ex>]static member inline is(__, f ) = match __ with Some' __ -> f __ | _ -> false
 [<Ex>]static member inline H (__, h ) = (^__:(member set_Height: _->u) __, h)
 [<Ex>]static member inline W (__, w ) = (^__:(member set_Width:  _->u) __, w) 
 [<Ex>]static member inline Sz(__, sz) = ExtsEx.W(__, (^__:(member Width:_)sz)); ExtsEx.H(__, (^__:(member Height:_)sz))


type[<Ex>]ContainerEx =
  [<Ex>]static member If (__:b sh, f) = fun g -> if __.v then f else g

