﻿[<AutoOpen>]
module FStap.Types


let shInits = ra()
let mutable private initing = false 
let addShInit f =
  if not initing then
    shInits <+ f

type 'a sh =
  abstract member v        : 'a with get,set
  abstract member fired    : 'a obs
  abstract member fire     : 'a -> u
  abstract member force    : 'a -> u
  abstract member quiet    : 'a -> u


type sv<'a when 'a:equality> (v:'a) as __ =
  let mutable _v = v
  let e = Event<'a>()
  let _force v = _v <- v; e.Trigger _v
  let _fire v = if _v <> v then _force v
  do addShInit <| fun _ -> _force _v
  interface 'a sh with
    member __.v with get () = _v and set v = _fire v
    member __.fired   = e.Publish :> _ obs
    member __.fire  v = _fire v
    member __.force v = _force v
    member __.quiet v = _v <- v
let sh __ = sv __ :> 'a sh
let toSh __ = __ :> _ sh

let INIT_SHARED_VALUES<'__> = 
  initing <- true
  for f in shInits do f ()
  initing <- false


type shs<'b,'a when 'b :> 'a sh and 'a:equality> (xs:'b seq) =
  let shs = [| for x in xs -> x :> 'a sh |]
  let mutable _v = Seq.head shs
  let _idx = sh 0
  do _idx.fired |> Observable.add (fun i -> _v <- Seq.item i shs)
  member __.x   = Seq.item _idx.v xs
  member __.xs  = xs
  member __.cur = _v
  member __.vs  = shs |> Array.map (fun sh -> sh.v)
  member __.V (v:'a sh) = Seq.findIndex ((=)v) shs |> _idx.fire
  member __.idx   = _idx.v
  member __.Idx i = _idx.fire i
  member __.idx'  = _idx
  member __.sh = __ :> 'a sh
  interface 'a sh with
    member __.v with get () = _v.v and set v = _v.fire v
    member __.fired      = 
      Observable.map (fun _ -> _v.v) _idx.fired ::
      [ for v in shs -> Observable.filter (fun _ -> v = _v) v.fired ]
      |> Seq.reduce Observable.merge 
    member __.fire v  = _v.fire  v
    member __.force v = _v.force v
    member __.quiet v = _v.quiet v


let mkShs<'b,'a when 'b :> 'a sh and 'a:equality> __ = shs<'b,'a> __


type opt'<'a> (?v:'a) =
  let mutable _v = v
  member __.v with get() = _v.Value and set v = _v <- Some v
  member __.v'     = _v
  member __.ok     = _v.IsSome
  member __.bad    = _v.IsNone
  member __.map  f = Option.map  f _v
  member __.iter f = Option.iter f _v
  member __.Or d   = defaultArg _v d
  member __.none   = _v <- None
  member __.Item f = __.map  f
  member __.Item f = __.iter f

let None'<'a> = opt'<'a> ()
let (|Some'|_|) (__:_ opt') = __.v'
