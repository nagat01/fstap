﻿[<AutoOpen>]
module FStap.Combinators
open System
open System.Threading
open Microsoft.FSharp.Reflection


let inline tee f x = f x; x
let inline ig _ = ()

let If<'a> b f g = if b then f   else (g:'a)
let bToOpt b x   = if b then Some x else None
let tupleToSome  = function true, v -> Some v | _ -> None

let inline (<+) m v = (^__:(member Add:_->_) m, v) |> ignore
let inline (!.) x = float x
let parseEnum<'a> s = Enum.Parse(typeof<'a>, s) :?> 'a
let enumToS (key:'a) = Enum.GetName(typeof<'a>, key).ToLower()
let cases<'a> = [| for x in FSharpType.GetUnionCases typeof<'a> -> FSharpValue.MakeUnion(x,[||]) :?> 'a |]
let tryCast<'a> (__:o) = match __ with :? 'a as __ -> Some __ | _ -> None

let inline getv __ = (^__:(member v:_) __) 


let Def<'a>  = Unchecked.defaultof<'a>
let Zero<'a> = Def<'a>


let x_  (x, _)    = x
let _x  (_, x)    = x
let x__ (x, _, _) = x
let _x_ (_, x, _) = x
let __x (_, _, x) = x
let a_ f (x, _)   = f x
let _a f (_, x)   = f x
let _f f   (a, b) = a  , f b
let f_ f   (a, b) = f a,   b
let ff f   (a, b) = f a, f b
let fg f g (a, b) = f a, g b


let rand = Random ()

let inline (%^) x y = if y = Zero then Zero else ((x % y) + y) % y

let inline (+=)  (a:_ byref) b = a <- a + b
let inline (-=)  (a:_ byref) b = a <- a - b
let inline (+=!) (a:_ byref) b = &a += b; a


// async
let immediate a = Async.StartImmediate a

#nowarn "40"
let awaitObs (obs:'a obs):'a a =
  let c1 = SynchronizationContext.Current
  Async.FromContinuations <| fun (cont,_,_) -> 
    let rec remover : IDisposable = obs.Subscribe handler
    and callback v =
      remover.Dispose()
      try cont v with _ -> ()
    and handler v =
      if c1 <> null && c1 <> SynchronizationContext.Current
      then c1.Post((fun _ -> callback v), null)
      else callback v
    ()