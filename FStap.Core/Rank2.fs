﻿[<AutoOpen>]
module FStap.Rank2
open System open System.Windows open System.Windows.Controls

open System.Collections.Generic
open System.Windows.Controls.Primitives
open System.Windows.Documents
open System.Windows.Media
open System.Windows.Shapes


type Or() =
  static member (?<-) (_:Or, m:_ opt,  x)     = defaultArg m x
  static member (?<-) (_:Or, m:_ opt', x)     = m.Or x
  static member (?<-) (_:Or, m:_ Nullable, x) = m.GetValueOrDefault x
let inline (|?) m x = Or() ? (m) <- x


type Holder() =
  static member inline (+) (_:Holder, r) = fun l -> l + r
  static member inline (-) (_:Holder, r) = fun l -> l - r
  static member inline (+) (l, _:Holder) = fun r -> l + r
  static member inline (-) (l, _:Holder) = fun r -> l - r
let _1 = Holder ()


type PrimaryValue() =
  static member (?) (_:PrimaryValue, m:_ ref       ) = m.Value
  static member (?) (_:PrimaryValue, m:ContentControl) = m.Content
  static member (?) (_:PrimaryValue, m:RangeBase   ) = m.Value
  static member (?) (_:PrimaryValue, m:ToggleButton) = m.IsChecked.ok
  static member inline (?) (_:PrimaryValue, m      ) = (^__:(member v:_)m)
let inline getpv    m = PrimaryValue() ? (m)
let inline (<-*)  f m = f (getpv m)
let inline (|*>)  m f = f  <-* m


type Equal() =
  static member inline (?<-) (_:Equal, a, b) = getpv a = getpv b
  static member inline (?<-) (_:Equal, a, b) = getpv a =       b
let inline (=.) a b = Equal() ? (a) <- b


type NotEqual() =
  static member inline (?<-) (_:NotEqual, a, b) = getv a <> getv b
  static member inline (?<-) (_:NotEqual, a, b) = getv a <>     b
let inline (<>.) a b = NotEqual() ? (a) <- b


type Assign() =                                          
  static member (?<-) (_:Assign, m,              x) = m x
  static member (?<-) (_:Assign, m:_ ra,         x) = m.Add x
  static member (?<-) (_:Assign, m:_ Queue,      x) = m.enq x
  static member (?<-) (_:Assign, m:_ Event,      x) = m.Trigger x
  static member (?<-) (_:Assign, m:Trigger,      x) = m.Setters  <+ x
  static member (?<-) (_:Assign, m:Panel,        x) = m.Children <+ x 
  static member (?<-) (_:Assign, m:ItemsControl, x) = m.Items    <+ x 
  static member (?<-) (_:Assign, m:ContentControl,x)= m.Content  <- x
  static member (?<-) (_:Assign, m:Decorator,    x) = m.Child    <- x
  static member (?<-) (_:Assign, m:TextBox,      x) = m.Text     <- x
  static member (?<-) (_:Assign, m:Image,        x) = m.Source   <- x
  static member (?<-) (_:Assign, m:MediaElement, x) = m.Source   <- x
  static member (?<-) (_:Assign, m:Polygon,      x) = m.Points   <+ x
  static member (?<-) (_:Assign, m:Polyline,     x) = m.Points   <+ x
  static member (?<-) (_:Assign, m:Path,         x) = m.Data     <- x
  static member (?<-) (_:Assign, m:PathGeometry, x) = m.Figures  <+ x
  static member (?<-) (_:Assign, m:PathFigure,   x) = m.Segments <+ x
  static member (?<-) (_:Assign, m:ToggleButton, x) = m.IsChecked <- Nullable x
  static member (?<-) (_:Assign, m:FlowDocument, x) = m.Blocks  <+ (x:Block)
  static member (?<-) (_:Assign, m:Paragraph,    x) = m.Inlines <+ (x:Inline) 
  static member (?<-) (_:Assign, m:Paragraph,    x) = m.Inlines <+ (x:UIElement)
  static member (?<-) (_:Assign, m:Paragraph,    x) = m.Inlines <+ (x:s)
  static member (?<-) (_:Assign, m:Span,         x) = m.Inlines <+ (x:Inline)
  static member (?<-) (_:Assign, m:Span,         x) = m.Inlines <+ (x:UIElement)
  static member (?<-) (_:Assign, m:Span,         x) = m.Inlines <+ (x:s)
  static member (?<-) (_:Assign, m:TextBlock,    x) = m.Inlines <+ (x:Inline)
  static member (?<-) (_:Assign, m:TextBlock,    x) = m.Inlines <+ (x:UIElement)
  static member (?<-) (_:Assign, m:TextBlock,    x) = m.Text <- x
  static member (?<-) (_:Assign, m:VisualCollection, x) = m <+ x 
  static member inline (?<-) (_:Assign, m:RangeBase, x) = m.Value <- !.x
  static member inline (?<-) (_:Assign, m,           x) = (^__:(member set_v:_->u) m, x)

let inline assign m v = Assign() ? (m) <- v
let inline v      v m = assign m v
let inline ($)    v m = assign m v; v
let inline (|>*)  v m = assign m v
let inline (<*-)  m v = assign m v
let inline (<*-?) m v = Option.iter (assign m) v
let inline (<*-*) m v = assign m <-* v
let inline (|%|*) m f = m  |%| assign f


type PrimaryEvent() =                                     
  static member (?) (_:PrimaryEvent, m:_ obs      ) = m 
  static member (?) (_:PrimaryEvent, m:_ Event    ) = m.Publish  
  static member (?) (_:PrimaryEvent, m:_ sh       ) = m.fired   
  static member (?) (_:PrimaryEvent, m            ) = (m:Threading.DispatcherTimer).Tick      
  static member (?) (_:PrimaryEvent, m:ButtonBase ) = m.Click   
  static member (?) (_:PrimaryEvent, m:MenuItem   ) = m.Click   
  static member (?) (_:PrimaryEvent, m:RangeBase  ) = m.ValueChanged   
  static member (?) (_:PrimaryEvent, m:Selector   ) = m.SelectionChanged 
  static member (?) (_:PrimaryEvent, m:TextBoxBase) = m.TextChanged 
  static member inline (?) (_:PrimaryEvent, m     ) = (^__:(member e:_) m)

let inline toObs m = PrimaryEvent() ? (m) :> _ obs
let inline (>=<) m n = Observable.merge (toObs m) (toObs n)
let inline (><)  m n = Observable.merge (map ig (toObs m)) (map ig (toObs n))
let inline (-??) m f = toObs m >%? f
let inline (-%%) m v = toObs m |%> fun _ -> v
let inline (=>)  m f = (toObs m).Add f
let inline (=>*) m n = m => assign n

