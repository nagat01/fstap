﻿[<AutoOpen>]
module FStap.Math
open System
open System.Windows


let (|GT|_|) sc __ = (__ > sc).some()
let (|LT|_|) sc __ = (__ < sc).some()

let (|GTEQ|_|) sc __ = (__ >= sc).some()
let (|LTEQ|_|) sc __ = (__ <= sc).some()


type TimeSpan with
  member __.tmsec   = __.TotalMilliseconds
  member __.tsec    = __.TotalSeconds
  member __.tmin    = __.TotalMinutes
  member __.thour   = __.TotalHours
  member __.sShort (fraction:b) =
    let isPositive = __.tsec >= 0. 
    let tsec = isPositive.If __.tsec -__.tsec
    let sign = isPositive.If "" "-"
    let int =
      match tsec with 
      | GTEQ 600. -> "mm':'ss"
      | GTEQ 60.  -> "m':'ss"  
      | GTEQ 10.  -> "ss"
      | _         -> "''s"       
    sign + __.S(int + fraction.If "':'f" "")
  member __.sJapanese =
    match __.tsec with
    | GTEQ 3600. -> "h'時間'm'分's'秒'"
    | GTEQ 600.  -> "m'分's'秒'"
    | GTEQ 60.   -> "m'分's'秒'"
    | GTEQ 10.   -> "s'秒'"
    | _         -> "s'秒'"
    |> __.S


let inline msec   __ = TimeSpan.FromMilliseconds !.__ 
let inline sec    __ = TimeSpan.FromSeconds      !.__ 

let now<'__> = DateTime.Now

let nowM (__:DateTime byref) = 
  let d = now - __ 
  __ <- now
  d


let inline dist   a b   = abs (a - b)

let inline max3 a b c   = max (max a b) c
let inline max4 a b c d = max (max3 a b c) d
let inline min3 a b c   = min (min a b) c
let inline mini a b     = int (min a b)
let inline maxi a b     = int (max a b)
let inline minmax a b   = min a b, max a b

let inline within        a b __ = max a __ |> min b
let inline isWithin      a b __ = a <  __ && __ <  b
let inline isWithinLess  a b __ = a <= __ && __ <  b

let isInTriangle a b = 
  isWithin 0. 1. a && 
  isWithin 0. 1. b && 
  isWithin 0. 1. (a+b)

let innerDiv a b ratio = a * ratio + b * (1. - ratio)

let inline sqr x = x * x

type Char with
  member __.i = int __

type Int32 with
  member __.i64 = int64 __
  member __.f   = !. __
  member __.y   = byte __
  member __.c   = char __
  member __.isEven = __ % 2 = 0
  member __.ms = msec !.__ 

type Double with
  member __.isNaN = Double.IsNaN __
  member __.ok    = not __.bad
  member __.bad   = Double.IsNaN __ || Double.IsInfinity __
  member __.i     = int __
  member __.ri    = int (round __)
  member __.frac  = __ - floor __
  member __.ceili = int (ceil __)
  member __.sec   = sec __
  member __.vexp = exp ((__ - 1.) * 4.605)

type private Windows.Size with
  member __.w = __.Width 
  member __.h = __.Height
type private Windows.Rect with
  member __.w = __.Width 
  member __.h = __.Height

// rand
module Rng =
  let i   a b = rand.Next(a,b)
  let f   a b = a + rand.NextDouble() * (b - a)


type Add() =
  static member (?<-) (_:Add, a:f,  b:i)    = a   + b.f
  static member (?<-) (_:Add, a:i,  b:f)    = a.f + b
  static member (?<-) (_:Add, a:Po, b:Po)   = Po(a.X + b.X, a.Y + b.Y)
  static member (?<-) (_:Add, a:Po, b:Vec)  = Po(a.X + b.X, a.Y + b.Y)
  static member (?<-) (_:Add, a:Po, b:Sz)   = Po(a.X + b.w, a.Y + b.h)
  static member (?<-) (_:Add, a:Rect, b:Vec) = Rect(a.X + b.X, a.Y + b.Y, a.w, a.h)
  static member inline (?<-) (_:Add, a,  b) = getv a +      b
  static member inline (?<-) (_:Add, a,  b) =      a + getv b
  static member inline (?<-) (_:Add, a,  b) = getv a + getv b
let inline (+.) a b = Add() ? (a) <- b

type Sub() =   
  static member (?<-) (_:Sub, f:f,  i:i)   = f   - i.f
  static member (?<-) (_:Sub, i:i,  f:f)   = i.f - f
  static member (?<-) (_:Sub, p:Po, q:Po)  = Po(p.X - q.X, p.Y - q.Y)
  static member (?<-) (_:Sub, p:Po, q:Sz)  = Po(p.X - q.w, p.Y - q.h)
  static member (?<-) (_:Sub, p:Sz, q:Vec) = Sz(p.w - q.X, p.h - q.Y)
  static member inline (?<-) (_:Sub, a:_ byref, b) = a <- a - b
  static member inline (?<-) (_:Sub, a, b  ) = getv a - getv b
  static member inline (?<-) (_:Sub, a, b  ) = a      - getv b
  static member inline (?<-) (_:Sub, a, b  ) = getv a - b
let inline (-.) a b = Sub() ? (a) <- b

type Div() =                                                
  static member (?<-) (_:Div, i0:i,    i1:i  ) = i0.f / i1.f
  static member (?<-) (_:Div, f:f,    i:i  ) = f   / i.f
  static member (?<-) (_:Div, i:i,    f:f )  = i.f / f
  static member (?<-) (_:Div, p:Po,   d:f  ) = Po (p.X/d,   p.Y/d)          
  static member (?<-) (_:Div, p:Po,   d:i  ) = Po (p.X/d.f, p.Y/d.f)          
  static member (?<-) (_:Div, p:Po,   d:Sz ) = Po (p.X/d.w, p.Y/d.h)          
  static member (?<-) (_:Div, s:Sz,   d:f  ) = Sz (s.w/d,   s.h/d)      
  static member (?<-) (_:Div, s:Sz,   t:Sz ) = Vec(s.w/t.w, s.h/t.h)
  static member (?<-) (_:Div, a:Rect, b:f  ) = Rect(a.X/b,   a.Y/b,   a.w/b,   a.h/b)
  static member (?<-) (_:Div, a:Rect, b:Vec) = Rect(a.X/b.X, a.Y/b.Y, a.w/b.X, a.h/b.Y)
  static member (?<-) (_:Div, a:TimeSpan, b:TimeSpan) = a.tmsec / b.tmsec
  static member inline (?<-) (_:Div, a, b  ) = getv a / getv b
  static member inline (?<-) (_:Div, a, b  ) = a      / getv b
  static member inline (?<-) (_:Div, a, b  ) = getv a / b
let inline (/.) a b = Div() ? (a) <- b

type Mul() =
  static member (?<-) (_:Mul, a:Po,   b:Po ) = Po (a.X*b.X, a.Y*b.Y)       
  static member (?<-) (_:Mul, a:Po,   b:f  ) = Po (a.X*b,   a.Y*b)       
  static member (?<-) (_:Mul, a:Po,   b:i  ) = Po (a.X*b.f, a.Y*b.f)       
  static member (?<-) (_:Mul, a:Po,   b:Vec) = Po (a.X*b.X, a.Y*b.Y)
  static member (?<-) (_:Mul, a:Vec,  b:f  ) = Vec(a.X*b,   a.Y*b)
  static member (?<-) (_:Mul, a:Sz,   b:f  ) = Sz (a.w*b,   a.h*b)
  static member (?<-) (_:Mul, a:Rect, b:f  ) = Rect(a.X*b, a.Y*b, a.w*b, a.h*b)
  static member (?<-) (_:Mul, a:Rect, b:Vec) = Rect(a.X*b.X, a.Y*b.Y, a.w*b.X, a.h*b.Y)
  static member (?<-) (_:Mul, a:i,    b:f  ) = a.f * b
  static member (?<-) (_:Mul, a:f,    b:i  ) = a   * b.f
  static member inline (?<-) (_:Mul, a, b  ) = getv a * getv b
  static member inline (?<-) (_:Mul, a, b  ) = a      * getv b
  static member inline (?<-) (_:Mul, a, b  ) = getv a * b
let inline ( *. ) a b = Mul() ? (a) <- b
