﻿[<AutoOpen>]
module FStap.SystemUtil
open System
open System.Diagnostics
open System.Globalization
open System.IO
open System.Threading
open System.Web


let SET_ENG_CULTURE<'__> =
  let th = Thread.CurrentThread
  th.CurrentCulture   <- CultureInfo "en-US"
  th.CurrentUICulture <- CultureInfo "en-US"


let (+/) p1 p2 = Path.Combine (p1, p2)
let pwd<'__>   = System.AppDomain.CurrentDomain.BaseDirectory


type String with
  member __.urlEncode  = HttpUtility.UrlEncode __
  member __.htmlDecode = HttpUtility.HtmlDecode  __


let processPWD exe = Process.Start(ProcessStartInfo(exe, WorkingDirectory=".")).ig

let launch path = trym "プログラムの起動に失敗しました。" { processPWD path }


type Stopwatch' () as __ =
  inherit Stopwatch() 
  let e = Event<u>()
  do
    __.Start()
  member __.restart = __.Restart(); e.Trigger()
  member __.stop    = __.Stop()
  member __.sec     = __.Elapsed.tsec
  member __.msec    = __.Elapsed.tmsec
  member __.isMSec ms = __.msec > float ms
  member __.restarted = e.Publish

