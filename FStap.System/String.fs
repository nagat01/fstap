﻿[<AutoOpen>]
module FStap.String
open System 
open System.IO


type String with                          
  member __.dir  = Path.GetDirectoryName __
  member __.name = Path.GetFileName __
  member __.nameNoExt = Path.GetFileNameWithoutExtension __
  member __.ext    = Path.GetExtension __
  member __.fileOk = File.Exists __
  member __.dirOk  = Directory.Exists __
  member __.pathOk = __.fileOk || __.dirOk

  member __.isReadOnly = File.GetAttributes(__).HasFlag FileAttributes.ReadOnly
 
  member __.files pats =
    if pats = "" 
    then Directory.GetFiles __
    else [| for pat in pats.Split '|' do yield! Directory.GetFiles(__, pat) |]

  member __.dirs       = Directory.GetDirectories __

  member __.read       = File.ReadAllText  __
  member __.readLines  = File.ReadAllLines __
  member __.write text = File.WriteAllText(__, text)
  member __.writeLines lines = File.WriteAllLines(__, Seq.toArray lines)

  member __.mkDir = if Directory.Exists(__).n then Directory.CreateDirectory(__).ig

