﻿[<AutoOpen>]
module FStap.Win32
open System.Diagnostics
open System.Runtime.InteropServices
open System.Text


// get
[<Dll "user32.dll">] extern 
  ni GetForegroundWindow()
[<Dll "user32.dll">] extern [<MarshalAs(UnmanagedType.Bool)>]
  b SetForegroundWindow(ni hWnd)

[<Dll "user32.dll">] extern 
  u32 GetWindowThreadProcessId(ni hWnd, u32& pid)
[<Dll("user32.dll", CharSet=CharSet.Auto)>] extern 
  i GetWindowTextLength(ni hWnd)
[<Dll("user32.dll", CharSet=CharSet.Auto)>] extern 
  i GetWindowText (ni hWnd, StringBuilder lpString, i nMaxCount)


let SW_SHOWNORMAL = 1
[<Dll "user32.dll">] extern b ShowWindow(ni hWnd, i nCmdShow)    


let fgExe<'__> =
  let hWnd = GetForegroundWindow()
  let mutable pid = 0u
  GetWindowThreadProcessId(hWnd, &pid).ig
  try Process.GetProcessById(int pid).MainModule.FileName with _ -> ""

let fgTitle<'__> =
  let h = GetForegroundWindow()
  let len = GetWindowTextLength h
  let sb = StringBuilder len
  GetWindowText(h, sb, len).ig
  sb.s
