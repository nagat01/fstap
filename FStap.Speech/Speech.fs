﻿[<AutoOpen>]
module FStap.Speech
open System.Globalization
open System.Speech.Synthesis

type VoiceInfo with
  member __.shortName =
    __.Name.remRegex "Microsoft Server Speech Text to Speech Voice |\(|\)|,"

type SpeechSynthesizer with
  member __.voices = 
    __.GetInstalledVoices() 
    |%> fun __ -> __.VoiceInfo
  member __.selVoice name =
    for voice in __.voices do
      if voice.Name.hasLower name then 
        __.SelectVoice voice.Name
  member __.getVoices (culture:s) = [
    for __ in __.voices do
      if __.Culture = (CultureInfo culture) then
        yield __ ]

type Speech' (culture:s, closing: u obs) as __ =
  let _speech = new SpeechSynthesizer()
  let _voices = _speech.getVoices culture
  do
    match _voices with
    | voice::_ -> _speech.selVoice voice.Name
    | _ -> ()
    closing =+! { _speech.Dispose() }
  member __.speech = _speech
  member __.selVoice name = _speech.selVoice name
  member __.voices = _voices
  member __.speak (s:s) = _speech.SpeakAsync s
  member __.volume with set volume = _speech.Volume <- volume
  member __.speed  with set speed  = _speech.Rate <- speed
  member __.wait = _speech.SpeakCompleted.wait
  member __.culture = culture

type VoiceInfoLabel(vi:VoiceInfo) =
  member   __.Name       = vi.Name
  override __.ToString() = vi.shortName

(*
type SpeechComboBox (ico, culture:s) as __ =
  inherit HStackPanel()
  let speech = new SpeechSynthesizer()
  let voices = query {
    for __ in speech.voices do
    where (__.Culture = CultureInfo culture)
    select (VoiceInfoLabel __) }
  let _   = ico   $ __
  let cmb = Cmb() $ __
  do
    cmb =+ { speech.selVoice (cmb.item :?> VoiceInfoLabel).Name }
    cmb |> itemsi 0 voices
    mw.Closing =+! { speech.Dispose() }
  member __.v = speech
  member __.e = cmb.SelectionChanged
  member __.speak (s:s) = speech.SpeakAsync s |> ig
  member __.wait = speech.SpeakCompleted.wait

type SpeechPanel(?interval, ?capacity) as __ =
  inherit StackPanel()
  let queue = Queue<s>()
  let english      = SpeechComboBox(Ico.english, "en-US")  $ __
  let japanese     = SpeechComboBox(Ico.japanese, "ja-JP") $ __
  let volInterval  = FloatVolume ("読み上げの間隔", 10., interval |? 0.)  $ __
  let volCapacity  = IntVolume   ("貯めこむ文章の最大数", 100, capacity |? 100) $ __
  let replace      = PTbx("単語の置き換え", "rwhois,あーるふーいず;#,sharp;・,;www,;ｗｗｗ,;WWW,;WWW,") $ __
  let remove       = PTbx("読み上げない文字", "()（）･´｀ω+ﾉ!:/") $ __
  let btnTerminate = Button' "切り上げる" $ __
  let educateTts (s:s) =
    let s = s.words.map(fun w -> w.urlOk.If "ゆーあーるえる省略" w).concat " "
    try s.repMany replace.kvs >%?~ remove.v.have |> String.Concat 
    with __ -> msg "読み上げる文字列を変換するためのjsonが間違っているようです。"; s
  do
    btnTerminate =+ { queue.Clear() }
    immediateLoop' 10 {
    do! invoke { V volInterval.v }
    while queue.Count > invoke { V volCapacity.v } do 
      queue.Dequeue().ig 
    if queue.Count > 0 then 
      let s = queue.Dequeue() |> educateTts
      let speech = s.isEnglish.If english japanese
      speech.speak s  
      do! speech.wait }
  member __.Speak s = queue.Enqueue s
  member __.Volume volume = 
    english.v.Volume <- volume
    japanese.v.Volume <- volume
*)
