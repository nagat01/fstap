open FStap open System open System.Windows open System.Windows.Controls
[<product "LifeGame"; version "0.0"; company "ながと"; copyright "Copyright © ながと 2014";
  title "LifeGame: ライフゲームのサンプル"; STAThread>] ()

let Alive = 0xff000000
let Dead  = 0xffffffff

let window = Window' "LifeGame"
let  image = Image() $ window
let   wbmp = mkWriteableBitmap 100 100 $ image

array 10000 (fun i -> 
  match i%100, i/100 with
  | 50, 51 | 51, 50 | 51, 51 | 51, 52 | 52, 50 -> Alive
  | _ -> Dead)
  |> wbmp.writeAll


tm10msec.Tick =+ {
  let pixels = wbmp.pixels
  pixels.mapi (fun i pixel ->
    let xo = i % 100
    let yo = i / 100
    let mutable count = 0
    for x in max 0 (xo-1) .. min 99 (xo+1) do
      for y in max 0 (yo-1) .. min 99 (yo+1) do
        if (x <> xo || y <> yo) && pixels.[x + y * 100] = Alive then
          count <- count + 1
    let isAlive = if pixel = Alive then count = 2 || count = 3 else count = 3
    If isAlive Alive Dead)
  |> wbmp.writeAll }


window.run
