﻿open System
open System.Windows
open System.Windows.Controls

open FStap

// model
let x   = sh 0
let y   = sh 0
let ans = sh 0

// view
let window      = Window() $ ttl "NagaLib Sample"
let  stackPanel = StackPanel() $ window
let   textBox1  = TextBox()    $ stackPanel 
let   textBox2  = TextBox()    $ stackPanel 
let   button    = Button()     $ stackPanel $ ctt "calc"
let   label     = Label()      $ stackPanel

// controller
textBox1 =+? { Int32.Parse textBox1.Text |>* x }
textBox2 =+? { Int32.Parse textBox2.Text |>* y }
button   =+  { ans <*- x +. y  }

ans =>* label

[<STAThread>]
window.run