# FStap

FStap is the all-in-one library for F# + WPF development.

You can write super succinct code by using computation-expressions for underfloor wiring the many boilerplates.

The usage of this library is simplified by using rank-n ad-hoc polymorphic combinators.

Since FStap does so many different things and revised many pitfalls found when I use it,

It would be extremely difficult to understand about the library's all the features from the documents.

Then I'm writing Cook-Book of FStap in FStap.Doc folder and making example applications in FStap.Samples folder.

Please see them if you curious about this library.

## COPYRIGHT NOTICE

This Library contains the work that is distributed under Apache License version 2.0

Please see [FStap.TypeProviders/LICENSE.txt](/FStap.TypeProviders/LICENSE.txt)


## LICENSE

Copylight 2014 © nagat01

This software is licensed under MIT License.

Please see LICENSE.txt in the folder same as this file.
