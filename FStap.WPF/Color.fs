﻿[<AutoOpen>]
module FStap.Co
open System 
open System.Windows.Media


let private addSharp (s:s) = s.StartsWith("#").If s ("#"+s) |> box 


type ToBrush () =
  static member (?) (_:ToBrush, m) = 
    let br = BrushConverter().ConvertFrom (addSharp m) :?> Brush
    br.Freeze()
    br
  static member (?) (_:ToBrush, m) = 
    let br = SolidColorBrush m
    br.Freeze()
    br
  static member (?) (_:ToBrush, m) = (m: Brush)
let inline br m = ToBrush() ? (m) 


type String with
  member __.co = ColorConverter().ConvertFrom (addSharp __) :?> Color
  member __.br = br __


type Color with 
  member __.br  = br __
  member __.str = ColorConverter().ConvertToString __


type [<Ex>] CoEx =                                                                                
  [<Ex>] static member inline bg (co, __) = (^__:(member set_Background:_->u) __, br co)
  [<Ex>] static member inline fg (co, __) = (^__:(member set_Foreground:_->u) __, br co)


type Brush with
  member __.co = (__ :?> SolidColorBrush).Color


let inline coIToF i = float i / 255.
let inline coFToI f = f * 255. |> uint32
let inline coIToY (i:i) = within 0 255 i |> byte
let inline coFToY f = within 0. 255. (f * 255.) |> byte

let inline iToA  (__:i) = (__ >>> 24) &&& 255
let inline iToR  (__:i) = (__ >>> 16) &&& 255
let inline iToG  (__:i) = (__ >>> 8 ) &&& 255
let inline iToB  (__:i) =  __         &&& 255
let inline iargb (__:i) = iToA __, iToR __, iToG __, iToB __

let inline argbToI a r g b = (a <<< 24) + (r <<< 16) + (g <<< 8) + b
let inline toI  (co:Color) = (int co.A <<< 24) + (int co.R <<< 16) + (int co.G <<< 8) + int co.B
let inline ofRgb     r g b = Color.FromRgb  (coIToY r, coIToY g, coIToY b)
let inline ofArgb  a r g b = Color.FromArgb (coIToY a, coIToY r, coIToY g, coIToY b)
let inline ofI       (i:i) = Color.FromArgb (iToA(i).y, iToR(i).y, iToG(i).y, iToB(i).y)
let inline ofRgbF    r g b = Color.FromRgb  (coFToY r, coFToY g, coFToY b)
let inline ofB4 a r g b = Color.FromArgb(a, r, g, b)
                   
let ofHsl h s l : Color =
  let v = if l <= 0.5 then l * (1.0 + s) else l + s - l * s
  if v = 0.0 then ofRgbF l l l else
    let m = l * 2.0 - v
    let sv = (v - m) / v
    let h = h * 6.0
    let sextant = h.i
    let vsf = v * sv * h.frac
    let mid1 = m + vsf
    let mid2 = v - vsf
    match sextant with
      | 0 -> v,    mid1, m
      | 1 -> mid2, v,    m
      | 2 -> m,    v,    mid1
      | 3 -> m,    mid2, v
      | 4 -> mid1, m,    v
      | _ -> v,    m,    mid1
    |||> ofRgbF


let ofHsv h s v =
  let inline within x = within 0. 1. x
  let h = within h * 6.
  let s = within s
  let v = within v
  let f = h.frac
  let p = v * (1. - s)
  let q = v * (1. - f * s)
  let t = v * (1. - (1. - f) * s)
  let r,g,b =
    match h.i with
    | 1 -> q, v, p
    | 2 -> p, v, t
    | 3 -> p, q, v
    | 4 -> t, p, v
    | 5 -> v, p, q
    | _ -> v, t, p
  ofRgbF r g b
 

type Color with
  member __.a  = int __.A
  member __.r  = int __.R
  member __.g  = int __.G
  member __.b  = int __.B
  member __.mapRgb r g b = ofB4 __.A (coIToY(r __.r)) (coIToY(g __.g)) (coIToY(b __.b))
  member __.setA a = ofB4 (coFToY a) __.R __.G __.B
  member __.i    = toI __
  member __.argb = __.a, __.r, __.g, __.b
  member __.hsv  = 
    let r, g, b = coIToF __.R, coIToF __.G, coIToF __.B
    let M = max3 r g b
    let m = min3 r g b
    let c = M - m
    let h = if M = r then (g-b)/c+6. elif M = g then (b-r)/c+2. else (r-g)/c+4.
    let s = if c = 0. then 0. else c / M
    h % 6. / 6., s, M

let inline density mul (co:Color) =
  let inline f (x:i) = float x * mul |> int
  let a,r,g,b = co.argb
  ofArgb (f a) (f r) (f g) (f b)

let inline blend (c0:Color) (c1:Color) =
  let a0, r0, g0, b0 = c0.argb
  let a1, r1, g1, b1 = c1.argb
  let f v0 v1 = (v0 * (255 - a1) + v1 * 255) / 255
  ofArgb (f a0 a1) (f r0 r1) (f g0 g1) (f b0 b1)

let inline blendi i0 i1 = blend (ofI i0) (ofI i1) |> toI

type Color with
  member __.density mul = density mul __
  member __.dark (d:i) = __.mapRgb (_1 - d) (_1 - d) (_1 - d)
  member __.inv = 
    let h, _, v = __.hsv
    let h = (h + 0.5) % 1.
    let v = (v * 255. + __.g.f < 255.).If 0.8 0.02
    ofHsl h 1. v
    
  
let getColorOnScreen (p:Po) =
  let dc = GetWindowDC 0
  let i = GetPixel(dc, p.ix, p.iy)
  ReleaseDC(0, dc).ig
  ofRgb (iToR i) (iToG i) (iToB i)


let getColorOnMouse<'__> = getColorOnScreen MousePos


type HsvCo () as __ =
  let mutable _h = 0.
  let mutable _s = 1.
  let mutable _v = 0.
  let mutable _co = ofHsv 0. 1. 0.
  let e = Event<Color>()
  let set() = 
    _co <- ofHsv _h _s _v
    e.Trigger _co
  let _quiet (co:Color) =
    let h,s,v = co.hsv
    if h.ok then _h <- h
    _s <- s
    _v <- v
  let _force (co:Color) =
    _quiet co
    set()
  let _fire (co:Color) = 
    if _co <> co then _force co
  do addShInit <| fun _ -> _force _co
  member __.h   = _h 
  member __.s   = _s 
  member __.v'  = _v 
  member __.H h = if _h <> h then _h <- h; set()
  member __.S s = if _s <> s then _s <- s; set()
  member __.V v = if _v <> v then _v <- v; set()
  member __.Hsv h s v = __.H h; __.S s; __.V v
  member __.hsv = _h, _s, _v
  member __.co  = _co
  member __.Co  co = _fire  co
  member __.Co' co = _force co
  member __.ini = __.Hsv 0. 1. 0.

  member __.Equals (x:HsvCo) =
    __.h  = x.h && 
    __.s  = x.s && 
    __.v' = x.v'

  interface Color sh with
    member __.v 
     with get () = __.co 
     and  set v  = _fire v
    member __.fired   = e.Publish :> _ obs
    member __.fire  v = _fire  v
    member __.force v = _force v
    member __.quiet v = _quiet v
