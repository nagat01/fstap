﻿[<AutoOpen>]
module FStap.Graphic
open System open System.Windows 
open System.Collections.Generic
open System.IO
open System.Windows.Interop
open System.Windows.Media
open System.Windows.Media.Imaging


// icon
module Icon =
  open System.Drawing
  let icoToImg (ico:Icon) =
    let r = R32 (0, 0, ico.Width, ico.Height)
    let src = Imaging.CreateBitmapSourceFromHIcon(ico.Handle, r, BitmapSizeOptions.FromEmptyOptions())
    Controls.Image(Source=src)
  let getIco = Icon.ExtractAssociatedIcon
  let IcoDir<'__> = getIco @"C:\Windows\explorer.exe"    |> icoToImg
  let IcoSln<'__> = getIco @"C:\P\DevTools\DevTools.sln" |> icoToImg
  let (!=) (a:Bitmap) (b:Bitmap) = 
    a.Width <> b.Width || 
    a.Height <> b.Height || 
    [ for y,x in cart2 [0..a.Height-1] [0..a.Width-1] -> a.GetPixel(x,y) <> b.GetPixel(x,y) ].any id
  let hasIco (ico:Icon) = ico.ToBitmap() != (getIco @"C:\P\FStap\FStap.Samples\LifeGame\bin\Debug\LifeGame.exe").ToBitmap()
  let tryGetImg path = try let ico = getIco path in hasIco ico >>= icoToImg ico with _ -> Some IcoDir
  let getImg path = getIco path |> icoToImg



// wb
type WriteLineArgs = { i:i; x:i; p:Po; }
type WriteableBitmap with
  member __.r32  = r32 0 0 __.pw __.ph
  member __.area = __.pw * __.ph

  member __.copyAll arr =
    __.CopyPixels(arr, __.pw * 4, 0)
  member __.copyRect (r32:R32) (arr:i[]) =
    __.CopyPixels(r32, arr, r32.w * 4, 0)

  member __.pixel (p:Po) =
    let arr = [|0|] 
    __.copyRect (r32 p.ix p.iy 1 1) arr
    Co.ofI arr.[0]

  member __.pixels =
    let pxs = arrayZero<i> __.area 
    __.copyAll pxs
    pxs

  member __.writeAll pxs = 
    __.writeRect __.r32 pxs

  member __.writeRect (r32:R32) (pxs:i[])  =
    __.WritePixels(r32, pxs, r32.w * 4, 0)

  member __.writeRectIdxArray (r:R32) (pxs:i[]) f = 
    for i in 0 .. pxs.ilast do
      pxs.[i] <- f i
    __.writeRect r pxs
  member __.writeRectXY (r:R32) f =
    __.writeRect r (Array.init r.area (fun i -> f (i % r.w) (i / r.w)))
  member __.writeRectPo (r:R32) f = 
    __.writeRectXY r (fun x y -> r.tl + vec x y |> f)
  member __.writeRectIdx (r:R32) f =
    __.writeRectXY r (fun x y -> (r.X + x) + (r.Y + y) * __.pw |> f)

  member __.writeLine (r32:R32) (arr:i[]) f =
    for x in 0 .. r32.w - 1 do
      let p = r32.tl + vec x 0
      let i = p.ix + p.iy * __.pw
      arr.[x] <- f {i=i; x=x; p=p}
    __.writeRect r32 arr

  member __.map f =
    for i in 0 .. __.pixels.ilast do 
      __.pixels.[i] <- f __.pixels.[i]
    __.writeRect __.r32 __.pixels


let mkWriteableBitmap w h = WriteableBitmap(w, h, 92., 92., PixelFormats.Pbgra32, null)


// rtb
let mkRtb (sz:Sz) = RenderTargetBitmap(sz.iw, sz.ih, 96., 96., PixelFormats.Pbgra32)

type RenderTargetBitmap with
  member __.render draw = 
    let dv = DrawingVisual()
    (use dc = dv.RenderOpen() in draw dc)
    __.Render dv


// bmp
type BitmapImage with
  member __.pixels = 
    Array.zeroCreate __.parea 
    $ fun arr -> __.CopyPixels(arr, __.pw * 4, 0)
    |%> Co.ofI
  member __.pixelsI =
    Array.zeroCreate __.parea
    $ fun (arr:i[]) -> __.CopyPixels(arr, __.pw * 4, 0)



let mkImageSource file = try_ {
  use stream = new MemoryStream(File.ReadAllBytes file)
  return WriteableBitmap(BitmapFrame.Create stream) }

let bimgOfPath path = BitmapImage (Uri path)

let private mkBmpImg opt url =
  let __ = BitmapImage()
  __.BeginInit()
  __.CacheOption   <- opt
  __.CreateOptions <- BitmapCreateOptions.IgnoreColorProfile
  __.UriSource     <- Uri url
  __.EndInit()
  __

let bimgSafe    url = mkBmpImg BitmapCacheOption.OnLoad url
let bimgNoCache url = mkBmpImg BitmapCacheOption.None   url
  
let mkThumbnail ma (__:BitmapSource) = 
  let sc = ma / (max __.pw __.ph).f
  TransformedBitmap(__, ScaleTransform(sc, sc))

let grayBmp bmp =
  let __ = FormatConvertedBitmap()
  __.BeginInit()
  __.Source <- bmp
  __.DestinationFormat <- PixelFormats.Gray8
  __.EndInit()
  __ :> BitmapSource


// cursor
open System.Runtime.InteropServices
open System.Security.Permissions
open Microsoft.Win32.SafeHandles

type [<Struct>] IconInfo = 
  val mutable fIcon    : b
  val mutable xHotspot : i
  val mutable yHotspot : i
  val mutable hbmMask  : ni
  val mutable hbmColor : ni
  
[<Dll "user32.dll">] extern [<MarshalAs(UnmanagedType.Bool)>] 
  b GetIconInfo (ni hIcon, IconInfo& pIconInfo)
[<Dll "user32.dll">] extern 
  b DestroyIcon (ni hIcon) 
[<SecurityPermission (SecurityAction.LinkDemand, UnmanagedCode = true)>]
type SafeIconHandle() as __ =
  inherit SafeHandleZeroOrMinusOneIsInvalid (true)
  override __.ReleaseHandle() = DestroyIcon __.handle
[<Dll "user32.dll">] extern 
  SafeIconHandle CreateIconIndirect (IconInfo& icon)

let queueHandle = Array.init 3 (fun _ -> Queue<SafeHandle>())
let bmpToCursor idx (bmp:Drawing.Bitmap) =
  let queue = queueHandle.[idx]
  let mutable icon = IconInfo (xHotspot=0, yHotspot=0, fIcon=false) 
  let h = bmp.GetHicon()
  GetIconInfo(h, &icon).ig
  DestroyIcon h |> ig
  while queue.Count > 10 do
    queue.deq.Dispose()
  let handle = CreateIconIndirect &icon
  queue.enq handle
  CursorInteropHelper.Create handle


// filer
let feToBitmapFrame (fe:FE) =
  if fe.rw >= 1. && fe.rh >= 1. then
    let rtb = mkRtb fe.rsz
    rtb.Render fe
    BitmapFrame.Create rtb |> Some
  else None
  
let saveFePng file fe =
  use stream = new FileStream(file, FileMode.Create)
  let enc = PngBitmapEncoder()
  feToBitmapFrame fe |%| fun frame -> 
    enc.Frames <+ frame
  enc.Save stream

