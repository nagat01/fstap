﻿[<AutoOpen>]
module FStap.DebugWindow
open System.Windows open System.Windows.Controls


type DebugWindow (wnd:Window) as __ =
  inherit Window(AllowsTransparency=true, WindowStyle=WindowStyle.None, Top=0., Left=0.)
  let  tbx = TextBox(FontSize = 12., Text = "   0: ") $ __
  do
    if __.IsVisible.n then try' { 
      __.Show()
      wnd.Left <- wscr - wnd.w
      wnd.Top  <- hscr - wnd.h
      wnd.Activate().ig }
  member __.Tbx = tbx


let mutable private line = 0
let private f (dw:DebugWindow) g fmt = Printf.kprintf (fun s -> invoke dw { g s; dw.Tbx.ScrollToEnd() }) fmt
let df  (dw:DebugWindow) fmt = f dw (fun s -> invoke dw { dw.Tbx.tf  " %s" s}) fmt 
let dfn (dw:DebugWindow) fmt = f dw (fun s -> &line += 1; invoke dw { dw.Tbx.tf " %s\r\n%-4d:" s line }) fmt 

