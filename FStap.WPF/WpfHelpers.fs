﻿[<AutoOpen>]
module FStap.WpfHelpers
open System.Windows open System.Windows.Controls  
open System.Windows.Controls.Primitives
open System.Windows.Documents
open System.Windows.Input
open System.Windows.Media
open System.Windows.Shapes

let uies (__:UIE seq) = __

let centerScreen  (__:Window) = __.WindowStartupLocation <- WindowStartupLocation.CenterScreen

let wToCtt (__:Window) = __.[ SizeToContent.Width  ]; __.[ SizeToContent.Manual ]
let hToCtt (__:Window) = __.[ SizeToContent.Height ]; __.[ SizeToContent.Manual ]
let szToCtt (__:Window) = __.[ SizeToContent.WidthAndHeight ]; __.[ SizeToContent.Manual ]

let lastChildFill (__:DockPanel) = __.LastChildFill <- true


// uie
let allowDrop    (__:UIE) = __.AllowDrop <- true
let clipToBounds (__:UIE) = __.ClipToBounds <- true
let nohit        (__:UIE) = __.IsHitTestVisible <- false

let inline itemsi idx (is:_ seq) (__:Selector) = is |%|* __; __.idx <- idx
let cs    (cs:UIE seq)  (__:Panel) = cs |%|* __
let cs'   (cs:#UIE seq) (__:Panel) = cs |%|  __.add

let remChildren<'a when 'a :> UIE> (__:Panel) =
  (extract __.cs :'a[]) |%| __.rem

let onEnter f (__:#UIE) = __.MouseEnter =+ { f __ }
let onLeave f (__:#UIE) = __.MouseLeave =+ { f __ }

let hoverOf (tg:#UIE) enter leave (__:#UIE) =
  __.MouseEnter =+ { enter tg }
  __.MouseLeave =+ { leave tg }
  leave tg

let inline opa opa __ = (__:UIE).Opacity <- !.opa
let inline opa2 enter leave (tg:UIE) (__:UIE) =
  hoverOf tg (fun tg -> tg.Opacity <- !.enter) (fun tg -> tg.Opacity <- !.leave) __
 
let inline light     opa    __ = opa2 opa 1   __ __
let inline light'    opa tg __ = opa2 opa 1   tg __
let inline discreet  opa    __ = opa2 1   opa __ __
let inline discreet' opa tg __ = opa2 1   opa tg __
 
let inline ttl x __ = (^__:(member set_Title  : s->u) __, x)
let inline ctt x __ = (^__:(member set_Content: _->u) __, x)
let inline hdr x __ = (^__:(member set_Header : _->u) __, x)
let inline src x __ = (^__:(member set_Source : _->u) __, x)

let inline w w   __ = (^__:(member set_Width    : f->u) __, !.w)
let inline h h   __ = (^__:(member set_Height   : f->u) __, !.h)
let inline mah h __ = (^__:(member set_MaxHeight: _->u) __, !.h)
let inline wh w' h'      __ = __ $ w w' |> h h'
let inline size  (sz:Sz) __ = wh sz.w sz.h __
let inline size1 x       __ = wh x x __
let inline sz'       obs __ = obs => __.Sz
let rect (r:Rect)        __ = (__:FE) $ r.TopLeft.mv |> size r.Size

let vis       (__:UIE)     = __.Visibility <- Visibility.Visible
let hidden    (__:UIE)     = __.Visibility <- Visibility.Hidden
let collapsed (__:UIE)     = __.Visibility <- Visibility.Collapsed
let visMany   (__:UIE seq) = __ |%| vis

let inline pad th       __ = (^__:(member set_Padding:Thickness->u) __, th)
let inline pad1 th'     __ = pad (th th')      __
let inline pad2 w h     __ = pad (th2 w h)     __
let inline padw w       __ = pad (th2 w 0)     __
let inline padh h       __ = pad (th2 0 h)     __
let inline pad4 l t r b __ = pad (th4 l t r b) __
let inline padl l       __ = pad (th4 l 0 0 0) __
let inline padt t       __ = pad (th4 0 t 0 0) __
let inline padr r       __ = pad (th4 0 0 r 0) __
let inline padb b       __ = pad (th4 0 0 0 b) __

let inline mgn th       __ = (^__:(member set_Margin:Thickness->u) __, th)
let inline mgn1 th'     __ = mgn (th th')      __
let inline mgn2 w h     __ = mgn (th2 w h)     __
let inline mgnw w       __ = mgn (th2 w 0)     __
let inline mgnh h       __ = mgn (th2 0 h)     __
let inline mgn4 l t r b __ = mgn (th4 l t r b) __
let inline mgnl l __ = mgn (th4 l 0 0 0) __
let inline mgnt t __ = mgn (th4 0 t 0 0) __
let inline mgnr r __ = mgn (th4 0 0 r 0) __
let inline mgnb b __ = mgn (th4 0 0 0 b) __

let private va va (__:FE) = __.VerticalAlignment <- va
let vaTop     __ = va VA.Top     __
let vaBottom  __ = va VA.Bottom  __
let vaCenter  __ = va VA.Center  __
let vaStretch __ = va VA.Stretch __

let private vca vca (__:Control) = __.VerticalContentAlignment <- vca
let vcaTop     __ = vca VA.Top     __
let vcaBottom  __ = vca VA.Bottom  __
let vcaCenter  __ = vca VA.Center  __
let vcaStretch __ = vca VA.Stretch __

let private ha ha (__:FE) = __.HorizontalAlignment <- ha
let haLeft    __ = ha HA.Left    __
let haRight   __ = ha HA.Right   __
let haCenter  __ = ha HA.Center  __
let haStretch __ = ha HA.Stretch __

let private hca ha (cc:ContentControl) = cc.HorizontalContentAlignment <- ha
let hcaLeft    __ = hca HA.Left    __
let hcaRight   __ = hca HA.Right   __
let hcaCenter  __ = hca HA.Center  __
let hcaStretch __ = hca HA.Stretch __

let aCenter  __ = vaCenter  __; haCenter  __
let caCenter __ = vcaCenter __; hcaCenter __
let aStretch __ = haStretch __; vaStretch __

let inline aTopLeft         __ = __ $ vaTop    |> haLeft
let inline aTopCenter       __ = __ $ vaTop    |> haCenter
let inline aTopRight        __ = __ $ vaTop    |> haRight
let inline aTopStretch      __ = __ $ vaTop    |> haStretch
let inline aCenterLeft      __ = __ $ vaCenter |> haLeft
let inline aCenterCenter    __ = __ $ vaCenter |> haCenter
let inline aCenterRight     __ = __ $ vaCenter |> haRight
let inline aCenterStretch   __ = __ $ vaCenter |> haStretch
let inline aBottomLeft      __ = __ $ vaBottom |> haLeft
let inline aBottomCenter    __ = __ $ vaBottom |> haCenter
let inline aBottomRight     __ = __ $ vaBottom |> haRight
let inline aBottomStretch   __ = __ $ vaBottom |> haStretch
let inline aStretchLeft     __ = __ $ vaStretch |> haLeft
let inline aStretchCenter   __ = __ $ vaStretch |> haCenter
let inline aStretchRight    __ = __ $ vaStretch |> haRight
let inline aStretchStretch  __ = __ $ vaStretch |> haStretch


// scroll bar
type Scr = ScrollBarVisibility
let inline private vscr vis __ = (^__:(member set_VerticalScrollBarVisibility: _->u)__, vis)
let inline vscrAuto __ = vscr Scr.Auto    __
let inline vscrVis  __ = vscr Scr.Visible __
let inline vscrHide __ = vscr Scr.Hidden  __

let inline private hscr vis __ = (^__:(member set_HorizontalScrollBarVisibility: _->u)__, vis)
let inline hscrAuto __ = hscr Scr.Auto    __
let inline hscrVis  __ = hscr Scr.Visible __
let inline hscrHide __ = hscr Scr.Hidden  __

let private _hscrVis' vis __ = ScrollViewer.SetHorizontalScrollBarVisibility (__, vis)
let hscrHide' __ = _hscrVis' Scr.Hidden __


// border
let inline corner radius (__:Border) = __.CornerRadius <- CornerRadius !.radius
let inline bdco co o = (^__:(member set_BorderBrush:Brush->u) o, br co)
let inline bdr4 l t r b co o =             
  bdco co o              
  (^__:(member set_BorderThickness:Thickness->u) o, th4 l t r b)
let inline bdr2 w h co o = bdr4 w h w h co o
let inline bdr  w   co o = bdr4 w w w w co o
let inline bdr' w r co o = bdr w co o; corner r o


// panel
let inline mv  x y __ = (po x y).mv __                                                                   
let inline mvx x   __ = (!.x).mvx __                                                                   
let inline mvy y   __ = (!.y).mvy __                                                                   
let zIdx idx __ = Panel.SetZIndex(__, idx)

let addColsStar stars (__:Grid) = for star in stars do __.addColStar star

// text
let inline fw fw      __ = (^__:(member set_FontWeight  :_->u) __, fw)
let inline fwi w      __ = fw (FontWeight.FromOpenTypeWeight w) __
let inline bold       __ = fw FontWeights.Bold __
let inline equalWidth __ = (^__:(member set_FontFamily  :_->u) __, FontFamily "Courier New")
let inline fsz sz     __ = (^__:(member set_FontSize    :f->u) __, !.sz)
let inline wrap       __ = (^__:(member set_TextWrapping:_->u) __, TextWrapping.Wrap)

// textbox
let multiline (__:TextBox) =
  __.AcceptsReturn <- true
  __ $ wrap $ vscrAuto |> ig

let scrollToText (text: TextElement) (rtb: RichTextBox) =
  let selection = rtb.Selection
  let start     = text.ElementStart
  selection.Select (start, start)
  let rect      = selection.Start.GetCharacterRect LogicalDirection.Forward
  let offset    = rect.Top + rtb.VerticalOffset - rtb.ah / 2.
  rtb.ScrollToVerticalOffset offset


let inline p0p1 p q     (ln:Line) = ln.p1<-p;  ln.p2<-q
let inline xy2 x0 y0 x1 y1 (ln:Line) = ln.p1 <- po x0 y0;  ln.p2 <- po x1 y1
let inline stroke th co (__:Shape) = __.StrokeThickness <- !.th; __.Stroke <- br co
let inline strokeTh  th (__:Shape) = __.StrokeThickness <- !.th
let inline strokeCo  co (__:Shape) = __.Stroke <- br co
let inline dotted xs    (__:Line ) = xs |> Seq.iter __.StrokeDashArray.Add
let inline fill co      (__:Shape) = __.Fill <- br co
let inline pnts (ps:_ seq) __      = (^__:(member set_Points:_->u)__,PointCollection ps)
let inline pnt3 a b c              = pnts [a;b;c]
let inline xys (xys:seq<_*_>) __   = pnts (xys |%> Point) __
let inline xys3 x0 y0 x1 y1 x2 y2  = xys [x0,y0; x1,y1; x2,y2]

let rounded (__:Shape) = __.StrokeLineJoin <- PenLineJoin.Round

let tip ctt (__:FE) = __.ToolTip <- ctt


let tr (p:Po) (__:UIE) =
  let tr = TranslateTransform (p.X, p.Y)
  match __.RenderTransform with
  | :? TransformGroup as g -> g.add tr
  | _ -> 
    let g = TransformGroup()
    g.add __.RenderTransform
    g.add tr
    __.RenderTransform <- g
let inline tr2 x y __ = tr (po x y) __
let inline trX x   __ = tr (po x 0) __
let inline trY y   __ = tr (po 0 y) __


// input
let poMouseOn __ = Mouse.GetPosition __

let isKeyDown = Keyboard.IsKeyDown
let isKeyUp   = Keyboard.IsKeyUp

let IsCtrl<'__>  = isKeyDown Key.LeftCtrl  || isKeyDown Key.RightCtrl
let IsShift<'__> = isKeyDown Key.LeftShift || isKeyDown Key.RightShift
let IsAlt<'__>   = isKeyDown Key.LeftAlt   || isKeyDown Key.RightAlt
let IsSpace<'__> = isKeyDown Key.Space


// media
type Scaling = BitmapScalingMode
let scaling mode __ = RenderOptions.SetBitmapScalingMode(__, mode)
let scHigh   __ = scaling Scaling.HighQuality __
let scNormal __ = scaling Scaling.Unspecified __
let scNearestNaighbor __ = scaling Scaling.NearestNeighbor __
let isScHigh b  = if b then scHigh else scNormal

let cursor cursor (__:FE) = __.Cursor <- cursor

let inline effect effect __ = (^__:(member set_Effect:_->u) __, effect)


// shape
let radRect x y (__:Rectangle) = 
  __.RadiusX <- 2.
  __.RadiusY <- 2.