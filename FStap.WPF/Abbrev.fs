﻿[<AutoOpen>]
module FStap.WPF
open System open System.Windows 
open System.Windows.Media.Imaging
open System.Windows.Threading
                   
type HA   = HorizontalAlignment
type VA   = VerticalAlignment
type FE   = FrameworkElement
type UIE  = UIElement
type Wb   = WriteableBitmap      

type DispatcherTimer'(ms:i) as __ =
  inherit DispatcherTimer() with
  do 
    __.Interval <- TimeSpan.FromMilliseconds(!. ms)

let mkTimer ms = 
  let timer = DispatcherTimer' ms
  timer.Start()
  timer

