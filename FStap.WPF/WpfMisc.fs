﻿[<AutoOpen>]
module FStap.WpfMisc
open System.Windows open System.Windows.Controls
open System.Windows.Controls.Primitives


// panel
type HStackPanel = 
  inherit StackPanel
  new ()    as __ = { inherit StackPanel( Orientation = Orientation.Horizontal ) } 
  new (ctt) as __ = new HStackPanel() then ctt |> __.add
  new (s:s) as __ = new HStackPanel(Label(Content = s))

type UniformGrid'(?c,?r) = 
  inherit UniformGrid (Columns=defaultArg c 1, Rows=defaultArg r 1)

type ComboBox'() = 
  inherit ComboBox (Style = (ToolBar().FindResource ToolBar.ComboBoxStyleKey :?> Style))


// label
type Label' =
  inherit Label
  new()      as __ = { inherit Label() } then __ $ pad1 0 $ vaCenter |> ig
  new(ttl:o) as __ = Label'() then __ |> ctt ttl

type TextBox'() as __ = 
  inherit TextBox()
  new (text) as __ = TextBox'() then __.Text <- text
  member __.v with get () = __.Text and set v = __.Text <- v

type TextBlock' (text) =
  inherit TextBlock(Text=text)


type ShadowedLabel(_fsz:f, ?_fgco:s, ?_s, ?_fw, ?_bdrco:s) as __ =
  inherit Grid()
  let _fw   = _fw |? FontWeights.UltraBold
  let _fgco = _fgco |? "000"
  let _s    = _s |? ""
  let labels = ra()
  let mkLabel () = Label' _s $ fsz _fsz $ fw _fw $ labels
  let label = mkLabel () $ _fgco.fg
  do 
    __ $ h (_fsz + 1.) |> cs' [ 
      for x,y in [-0.8,0.; 0.8,0.; 0.,-0.8; 0.,0.8] ->
        mkLabel () $ (_bdrco |? "fff").fg $ tr2 x y 
      yield label ]
  member __.v  with set (s:s)  = for label in labels do label <*- s
  member __.co with set (co:s) = label |> co.fg


// button
type Btn (ctt) = 
  inherit Button (Content=ctt)
  new () = Btn null


type Slider' (mi, ma, ?v:f, ?w, ?freq) as __ = 
  inherit Slider (Minimum=mi, Maximum=ma)
  do
    __ <*-? v
    __.W <*-? w
    __ |> vaCenter
    __.set_TickFrequency <*-? freq

type MediaElement' () = 
  inherit MediaElement(
    LoadedBehavior   = MediaState.Manual, 
    UnloadedBehavior = MediaState.Close)