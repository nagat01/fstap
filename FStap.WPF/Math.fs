﻿[<AutoOpen>]
module FStap.Math
open System open System.Windows
open System.Windows.Media
open System.Windows.Media.Imaging
open System.Windows.Shapes


// geometry //
let inline po  x y = Po  (!. x, !. y)  
let inline vec x y = Vec (!. x, !. y)
let inline sz  w h = Sz  (!. w, !. h)
let inline r32 x y w h = R32(int x, int y, int w, int h)

let poX  (p:Po) = p.X
let poY  (p:Po) = p.Y

let (|Po|)   (p:Po)  = p.X, p.Y


type Vector with
  member __.atan2 = Math.Atan2(__.Y, __.X)
  member __.normalize = __.Normalize(); __
  member __.angle v = Vector.AngleBetween(__, v)


type Point with
  member __.vec = vec __.X __.Y
  member __.ix  = __.X.i
  member __.iy  = __.Y.i
  member __.dist (p:Po) = sqr(__.X - p.X) + sqr(__.Y - p.Y) |> sqrt
  member __.mid (p:Po) = po ((__.X + p.X) / 2.) ((__.Y + p.Y) / 2.)
  member __.ratio (p0:Po) (p1:Po) =
    let v = p1 - p0
    if v.Length > 0. then (__ - p0) * v / v.LengthSquared else 0.

  member __.footLineSegment (p0:Po) (p1:Po) =
    let v = p1 - p0
    let u = __.ratio p0 p1
    p0 + (within 0. 1. u) * v

  member __.coord (o:Po) (a:Po) (b:Po) = 
    let a, b, c = a - o, b - o, __ - o
    let f (a:Vec) (b:Vec) (c:Vec) =
      if b.Y <> 0. then
        let mb = b.X / b.Y
        (c.X - c.Y * mb) / (a.X - a.Y * mb)
      elif a.Y <> 0. then
        c.Y / a.Y
      else 0.
    f a b c, f b a c

  member __.isInTriangle o a b =
    __.coord o a b ||> isInTriangle

let intersection (Po(x1,y1)) (Po(x2,y2)) (Po(x3,y3)) (Po(x4,y4)) =
  let vx0 = x1 - x2
  let vx1 = x3 - x4
  let vy0 = y1 - y2
  let vy1 = y3 - y4
  let d0  = x1 * y2 - y1 * x2
  let d1  = x3 * y4 - y3 * x4
  let div = vx0 * vy1 - vy0 * vx1
  let x = (d0 * vx1 - d1 * vx0) / div
  let y = (d0 * vy1 - d1 * vy0) / div
  po x y

let isIntersect margin (Po(x0,y0)) (Po(x1,y1)) (Po(x2,y2)) (Po(x3,y3)) =
  let isWithin = isWithin (0. + margin) (1. - margin)
  let x01 = x1 - x0
  let x02 = x2 - x0
  let x23 = x3 - x2
  let y01 = y1 - y0
  let y02 = y2 - y0
  let y23 = y3 - y2
  let d   = x01 * y23 - x23 * y01
  if d = 0. then
    false
  else
    let s = ( y01 * x02 - x01 * y02 ) / d
    let t = ( y23 * x02 - x23 * y02 ) / d
    isWithin s && isWithin t

let poAve (ps:Po seq) = po (Seq.averageBy poX ps) (Seq.averageBy poY ps)
let poInnerDiv (p:Po) (q:Po) ratio = po (innerDiv p.X q.X ratio) (innerDiv p.Y q.Y ratio)


// wpf
type UIElement with                  
  member __.rw   = __.RenderSize.Width
  member __.rh   = __.RenderSize.Height
  member __.rsz  = sz __.rw __.rh
  member __.szOk = __.rw.ok && __.rh.ok && __.rw >= 1. && __.rh >= 1.

type FrameworkElement with
  member __.w with get () = __.Width  and set w = __.Width  <- w
  member __.h with get () = __.Height and set h = __.Height <- h
  member __.aw  = __.ActualWidth
  member __.ah  = __.ActualHeight
  member __.asz = sz __.aw __.ah
  member __.inside (p:Po) = po (within 0. __.w p.X) (within 0. __.h p.Y)

type Size with
  member __.w     = __.Width
  member __.iw    = __.Width.i
  member __.h     = __.Height
  member __.ih    = __.Height.i
  member __.po    = po  __.w __.h

type BitmapSource with
  member __.pw    = __.PixelWidth
  member __.ph    = __.PixelHeight
  member __.psz   = sz __.pw.f __.ph.f
  member __.parea = __.pw * __.ph
  member __.ok    = try __.pw > 1 && __.ph > 1 with __ -> false

type Int32Rect with 
  member __.w  = __.Width
  member __.h  = __.Height

  member __.r  = __.X + __.w
  member __.b  = __.Y + __.h
  member __.tl = po __.X __.Y
  member __.br = po __.r __.b

  member __.sz = sz __.w __.h
  member __.area = __.w * __.h

  member __.ys = seq { __.Y .. __.b }

  member __.ok = __.w > 0 && __.h > 0

  member __.inside (_r:R32) =
    let x = max __.X _r.X
    let y = max __.Y _r.Y
    let r = min __.r _r.r
    let b = min __.b _r.b
    let w = r - x
    let h = b - y
    r32 x y w h


type Rect with
  member __.w   = __.Width
  member __.h   = __.Height
  member __.r32 = R32(__.X.i, __.Y.i, __.w.i, __.h.i)
  member __.trimedRect x y w h =
    let _x = within x (x + w - 1.) __.X 
    let _y = within y (y + h - 1.) __.Y
    let _w = within 1. (w + x - _x) (__.w + __.X - _x)
    let _h = within 1. (h + y - _y) (__.h + __.Y - _y)
    Rect(_x, _y, _w, _h)
  member __.trimedR32 x y w h = (__.trimedRect x y w h).r32


module Rect =
  let ofPo2 (p:Po) (q:Po) = Rect(min p.X q.X, min p.Y q.Y, dist p.X q.X, dist p.Y q.Y)
  let ofPos (ps:Po seq) = 
    let xs = ps |%> poX
    let ys = ps |%> poY
    let xmi, xma = Seq.min xs, Seq.max xs
    let ymi, yma = Seq.min ys, Seq.max ys
    Rect(xmi, ymi, xma - xmi, yma - ymi)


module R32 =
  let ofPo2 p q = (Rect.ofPo2 p q).r32
  let ofPos ps  = (Rect.ofPos ps).r32
  let hasArea (__:R32) = __.HasArea


// shapes
type Line with
  member __.p1 with get () = po __.X1 __.Y1 and set(p:Po) = __.X1 <- p.X; __.Y1 <- p.Y
  member __.p2 with get () = po __.X2 __.Y2 and set(p:Po) = __.X2 <- p.X; __.Y2 <- p.Y


// transform
type TransformGroup with 
  member __.add tr = __.Children.Add(tr).ig

type ScaleTransform with 
  member __.Scale with set (x,y) = __.ScaleX <- x; __.ScaleY <- y
  
type[<Ex>] MathEx () =
  [<Ex>]
  static member inline center(__, p:Po) = 
    (^__:(member set_CenterX:_->u) __, p.X)
    (^__:(member set_CenterY:_->u) __, p.Y)

let rotator angle = 
  let m = Matrix.Identity
  m.Rotate angle
  m

// math
let isConvex (ps:Po[]) =
  let e = ps.len - 1
  let angles = [ for i in 0..e -> let p d = ps.[(i+d)%(e+1)] in (p 0 - p 1).atan2 - (p 1 - p 2).atan2 ]
  let cnt a b = (angles >%? isWithin (a*Math.PI) (b*Math.PI)).len
  cnt 0. 1. = e || cnt 1. 2. = e


let inline th4 l t r b = Thickness (!. l, !. t, !. r, !. b)
let inline th2 w h     = th4 w h w h
let inline th th       = Thickness (!. th)


// graphic
open System.Runtime.InteropServices
#nowarn "9"
[<Struct; StructLayout(LayoutKind.Sequential)>]
type Win32Point = 
  val X : i
  val Y : i
[<Dll "gdi32" >] extern 
  i GetPixel(i hdc, i x, i y)
[<Dll "user32">] extern 
  i GetWindowDC(i hwnd)
[<Dll "user32">] extern 
  i ReleaseDC(i hWnd, i hDC)
[<Dll "user32"; MarshalAs(UnmanagedType.Bool)>] extern 
  b GetCursorPos(Win32Point& pt)


let MousePos<'__> =
    let mutable _po = Win32Point()
    GetCursorPos &_po |> ig
    po _po.X _po.Y
