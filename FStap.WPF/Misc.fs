﻿[<AutoOpen>]
module FStap.Misc
open System.Windows


let msg s        = MessageBox.Show s |> ig
let mf fmt       = Printf.kprintf msg fmt
let msgq ttl msg = MessageBoxResult.OK = MessageBox.Show(msg, ttl, MessageBoxButton.OKCancel, MessageBoxImage.Question)

let invoke<'a> (wnd:Window) = FuncBuilder<u, 'a, 'a>(fun f -> tryd { V (wnd.Dispatcher.Invoke f) })
