﻿[<AutoOpen>]
module FStap.WpfExts
open System open System.Windows open System.Windows.Controls
open System.Windows.Controls.Primitives
open System.Windows.Documents
open System.Windows.Input      
open System.Windows.Interop
open System.Windows.Media           
open System.Windows.Media.Imaging
open System.Windows.Threading
open System.Diagnostics

let mutable mw = Zero<Window>


type UIElement with
  member __.vis    = __.IsVisible
  member __.Vis  b = __.Visibility <- If b Visibility.Visible Visibility.Collapsed
  member __.Vis' b = __.Visibility <- If b Visibility.Visible Visibility.Hidden 
  member __.offset = __.TranslatePoint (po 0 0, VisualTreeHelper.GetParent __ :?> UIE)
  member __.rtr with get () = __.RenderTransform and set v = __.RenderTransform <- v


type FrameworkElement with
  member __.isHaLeft   = __.HorizontalAlignment = HA.Left
  member __.isHaRight  = __.HorizontalAlignment = HA.Right
  member __.isVaTop    = __.VerticalAlignment   = VA.Top
  member __.isVaBottom = __.VerticalAlignment   = VA.Bottom
  member __.sized = __.SizeChanged


// panel
type Panel with 
  member __.cs           = [| for x in __.Children -> x |]
  member __.indexOf      uie = __.cs.idx((=)uie)
  member __.add      uie = __ <*- uie
  member __.addHead  uie = __.Children.Insert(0, uie)
  member __.rem      uie = __.Children.Remove uie
  member __.clear        = __.Children.Clear ()

type ItemsControl with
  member __.is    = seq { for x in __.Items -> x }
  member __.cnt   = __.is.len
  member __.rem x = __.Items.Remove x
  member __.clear = __.Items.Clear()
  member __.swap a b =
    let __ = __.Items
    let _a = __.[a]
    let _b = __.[b]
    __.[a] <- TabItem()
    __.[b] <- _a
    __.[a] <- _b

type Double with
  member __.mvx uie = Canvas.SetLeft(uie, __)
  member __.mvy uie = Canvas.SetTop (uie, __)

type Point with 
  member __.mv uie  = __.X.mvx uie; __.Y.mvy uie

type UIElement with 
  member __.po     = po (Canvas.GetLeft __) (Canvas.GetTop __)
  member __.Po po  = (po:Po).mv __ 

type Vector with 
  member __.mv (uie:UIE) = (uie.po.X.ok.If uie.po (po 0 0) + __).mv uie

type Canvas with         
  member __.addTo (p:Po) uie = (__ :> Panel) <*- uie; p.mv uie
  member __.xy       x y uie = __.addTo (po x y) uie

type Grid with
  member __.addTo (?col,?row,?cspan,?rspan) = fun uie ->
    col  |%| fun n -> Grid.SetColumn     (uie, n)
    row  |%| fun n -> Grid.SetRow        (uie, n)
    cspan|%| fun n -> Grid.SetColumnSpan (uie, n)
    rspan|%| fun n -> Grid.SetRowSpan    (uie, n)
    __.add uie
  member __.cd = __.ColumnDefinitions
  member __.rd = __.RowDefinitions
  member __.addColStar ratio = ColumnDefinition() $ wStar ratio |> __.cd.Add
  member __.allColStar  = __.ColumnDefinitions.sumBy(fun cd -> cd.Width.Value)
  member __.fillColStar = __.addColStar (max 0. (1. - __.allColStar))

type DockPanel with
  member private __.addTo dock uie = DockPanel.SetDock(uie $ __,dock)
  member __.top    uie = __.addTo Dock.Top    uie
  member __.bottom uie = __.addTo Dock.Bottom uie
  member __.left   uie = __.addTo Dock.Left   uie
  member __.right  uie = __.addTo Dock.Right  uie

type Selector with
  member __.item 
    with get () = __.SelectedItem
    and  set o  = __.SelectedItem <- (o:obj)
  member __.idx 
    with get () = __.SelectedIndex
    and  set i  = __.SelectedIndex <- i
  member __.selNext = if __.cnt > 0 then __.idx <- (__.idx + 1) %  __.cnt
  member __.selPrev = if __.cnt > 0 then __.idx <- (__.idx - 1) %^ __.cnt

type RangeBase with
  member __.v  with get () = __.Value   and set v = __.Value   <- v
  member __.vexp = exp ((__.v - 1.) * 4.605)
  member __.ratio = __.v / __.Maximum

type TabControl with
  member __.addHdr'    hd item = let tabi = TabItem(Content=item, Header=hd) $ __ in item, tabi
  member __.addHdr     hd item = __.addHdr' hd item |> ig
  member __.addHdrSel' hd item = __.addHdr' hd item $ fun (_, tabi) -> (hd:UIE).MouseEnter =+ { __.item <- tabi }
  member __.addHdrSel  hd item = __.addHdrSel' hd item |> ig

type HeaderedContentControl with
  member __.hdr item = __.Header <- item


// controls
type MediaElement with
  member __.po with get () = __.Position and  set v  = __.Position <- v
  member __.progress  = __.po.tmsec / __.NaturalDuration.TimeSpan.tmsec
  member __.hasTs     = __.NaturalDuration.HasTimeSpan
  member __.isPlaying = (__.po - (Threading.Thread.Sleep 10; __.po)).tmsec < 0.
  member __.playPath path = 
    __ <*- Uri(path, UriKind.Relative)
    __.Play()

type IInputElement with
  member __.mpos = Mouse.GetPosition __


// input
let lPressed<'__> = Mouse.LeftButton   = MouseButtonState.Pressed
let rPressed<'__> = Mouse.RightButton  = MouseButtonState.Pressed
let mPressed<'__> = Mouse.MiddleButton = MouseButtonState.Pressed

let lOnlyPressed<'__> = lPressed   && mPressed.n && rPressed.n
let mOnlyPressed<'__> = lPressed.n && mPressed   && rPressed.n
let rOnlyPressed<'__> = lPressed.n && mPressed.n && rPressed

let mbOnlyPressed = function
  | MouseButton.Left   -> lOnlyPressed
  | MouseButton.Middle -> mOnlyPressed
  | MouseButton.Right  -> rOnlyPressed
  | _         -> failwith "unreacheable"

type[<Ex>] InputEx =
  [<Ex>]static member key (__:KeyEventArgs obs, key) = __ -?? (fun e -> e.Key = key)
  [<Ex>]static member inline posOn(__, x) = (^__:(member GetPosition: _ -> Po)(__, x))

type MouseEventArgs with
  member __.pos      = __.posOn (__.Source :?> IInputElement)
  member __.pressure iie =
    if __.StylusDevice.ok 
    then Seq.max [ for p in __.StylusDevice.GetStylusPoints iie -> float p.PressureFactor ]
    else 1.


type UIElement with
  member __.mmove  = __.MouseMove
  member __.menter = __.MouseEnter
  member __.mleave = __.MouseLeave
  member __.mup    = __.MouseUp
  member __.mdown  = __.MouseDown
  member __.pmmove  = __.PreviewMouseMove
  member __.pmup    = __.PreviewMouseUp
  member __.pmdown  = __.PreviewMouseDown
  member __.pmwheel = __.PreviewMouseWheel
  member __.hover = __.pmmove >< __.menter

  member __.lmdown = __.mdown -?? (fun e -> e.ChangedButton = MouseButton.Left )
  member __.rmdown = __.mdown -?? (fun e -> e.ChangedButton = MouseButton.Right)
  member __.lmup   = __.mup -?? (fun e -> e.ChangedButton = MouseButton.Left   )
  member __.rmup   = __.mup -?? (fun e -> e.ChangedButton = MouseButton.Right  )

  member __.kup    = __.KeyUp
  member __.kdown  = __.KeyDown
  member __.pkup   = __.PreviewKeyUp
  member __.pkdown = __.PreviewKeyDown

  member __.ku  k = __.kup.key    k
  member __.kd  k = __.kdown.key  k
  member __.pku k = __.pkup.key   k
  member __.pkd k = __.pkdown.key k

  member __.ktoggle k = __.ku k -%% false >=< __.kd k -%% true

  member __.enter = __.kd Key.Enter

  member __.focus    = __.Focus().ig                                                   

type UIElement with
  member __.lClick(hEnter, hDown, hUp) =
    let last = Stopwatch()
    __.lmdown =+ { last.Restart(); hDown() }
    __.MouseLeave =+ { hUp() }
    __.MouseEnter =+ { hEnter() }
    __.lmup -?? (fun _ -> hUp(); last.IsRunning && last.Elapsed < msec 500)
  member __.lclick = __.lClick(id,id,id)

type[<Ex>] UIEEx = 
  [<Ex>]
  static member inline lClick' (__:#UIE) =
    let bg = (^__:(member Background:Brush)__)
    let f (co:Brush) _ = co.bg __
    __.lClick(f "def".br, f "fdd".br, f bg)

type DragEventArg = 
  { Orig : Po
    Prev : Po
    Cur  : Po 
    Vec  : Vec
    vx   : f 
    vy   : f
    Pres : f opt * f opt }


type private c = Choice<MouseEventArgs,u>
type UIElement with
  member __.drag (pa:UIE) (mb:MouseButton) (gap:f) =
    let timer   = DispatcherTimer()
    let evTimer = Event<u>()
    let enable  = ref false
    if gap.ok then
      timer.Interval <- msec gap
      timer.Start()
    timer =+ {
      if !enable then
        evTimer.Trigger()
      enable := true }
    let changed (e:MouseButtonEventArgs) = e.ChangedButton = mb
    let up   _ = (mbOnlyPressed mb).n
    let down _ = mbOnlyPressed mb
    let ev   = Event<_>()
    let mutable orig = None 
    let mutable prev = None
    let mutable pres = None
    let getPres (e:MouseEventArgs) = try_ { yield e.pressure pa }
    let reset () = 
      orig <- None
      prev <- None
      pres <- None
    __.pmdown => fun e ->
      orig <- Some(e.posOn pa)
      prev <- Some(e.posOn pa)
      pres <- getPres e
    (mw.mup -?? changed) >< 
    (mw.MouseMove -?? up) ><
    mw.MouseLeave =+ { reset () }
    ((mw.MouseMove |%> c.Choice1Of2) >=< 
     (evTimer.Publish |%> c.Choice2Of2)
    ) -?? down => fun e ->
      let _pres = 
        match e with 
        | Choice1Of2 e -> 
          enable := false
          getPres e 
        | Choice2Of2 _ -> 
          pres
      let cur = Mouse.GetPosition pa
      if orig.ok then 
        let vec = cur - prev.v
        ev <*- { 
          Orig = orig.v
          Prev = prev.v
          Cur  = cur
          Vec  = vec
          vx   = vec.X
          vy   = vec.Y
          Pres = pres, _pres }
        prev <- Some cur
        pres <- _pres
    ev.Publish

  member __.dragm pa mb  = __.drag pa mb
  member __.lDragAbs     = __.dragm mw MouseButton.Left   nan
  member __.rDragAbs     = __.dragm mw MouseButton.Right  nan
  member __.mDragAbs     = __.dragm mw MouseButton.Middle nan
  member __.lDrag        = __.dragm __ MouseButton.Left   nan
  member __.rDrag        = __.dragm __ MouseButton.Right  nan
  member __.lDragOn x    = __.dragm x  MouseButton.Left   nan
  member __.rDragOn x    = __.dragm x  MouseButton.Right  nan
  member __.lDragGap gap = __.dragm __ MouseButton.Left   gap
  member __.dragAndDown  = __.mdown >< __.lDrag -? { V lOnlyPressed } 

type Window with
  member __.drag =
    let mutable prev = None
    __.MouseMove |%> fun e ->
      let cur = e.posOn __
      let vec = cur - (prev |? cur)
      prev <- Some cur
      vec
  member __.dragOf key =
    __.drag -? { V (Keyboard.IsKeyDown key) }
 
let MBtns<'__>      = [Mouse.LeftButton; Mouse.MiddleButton; Mouse.RightButton; Mouse.XButton1; Mouse.XButton2]
let noMBtnDown<'__> = MBtns.forall((=)MouseButtonState.Released)

// misc
type Window with
  member __.run = (Application().Run __).ig
  member __.po with get () = po __.Left __.Top and set (p:Po) = __.Left <- p.X; __.Top <- p.Y
  member __.Po po = __.po <- po
  member __.Item x = __.SizeToContent <- x
  member __.Item x = __.WindowStyle <- x
  member __.Item x = __.WindowState <- x
  member __.minimize = __.[ WindowState.Minimized ]
  member __.normal   = __.[ WindowState.Normal ]
  member __.none     = __.[ WindowStyle.None ]
  member __.bordered =__.[ WindowStyle.SingleBorderWindow ]
  member __.toForeground =
    __.normal
    __.Topmost <- true
    __.Topmost <- false
  member __.activate = __.Activate().ig
  member __.activatedChanged =
    (__.Activated >< __.Loaded) -%% true >=<
    __.Deactivated -%% false
  member __.focusedChanged =
    (__.MouseEnter >< __.Activated) -%% true >=<
    (__.MouseLeave -? { Not __.IsActive } >< __.Deactivated) -%% false
  member __.handle = WindowInteropHelper(__).Handle

type ToggleButton with
  member __.v with get () = __.IsChecked.ok and set v = __.IsChecked <- Nullable v

type TextBox with
  member __.redirected (wnd, exe, args, ?cmd:s, ?dir) =
    let psi = ProcessStartInfo(exe, args, CreateNoWindow=true, UseShellExecute=false, RedirectStandardInput=true, RedirectStandardOutput=true)
    dir |%| psi.set_WorkingDirectory
    let p = Process.Start psi
    p.BeginOutputReadLine()
    p.OutputDataReceived => fun e -> invoke wnd { __.tfn "%s" e.Data }
    cmd |%| __.tfn "%s"
    cmd |%| p.StandardInput.WriteLine

type RichTextBox with
  member __.doc   = __.Document
  member __.start = __.doc.ContentStart
  member __.end'  = __.doc.ContentEnd
  member __.range = TextRange(__.start, __.end')
  member __.text  = __.range.Text
  member __.prop prop v = __.Selection.ApplyPropertyValue(prop, v)


// media
type PointCollection with
  member __.clear = __.Clear()

type Image with
  member __.v with get () = __.Source and set v = __.Source <- v
  member __.bmp = __.v :?> BitmapSource
  member __.bmpOk = tryd { yield __.bmp.pw > 1 }


// type extension
type [<Ex>] WpfEx =
  [<Ex>] static member Vis(__:#FE seq, b) = for __ in __ do __.Vis b

