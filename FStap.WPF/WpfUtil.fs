﻿[<AutoOpen>]
module FStap.WpfUtil
open System.Windows
open System.Windows.Controls.Primitives
open Printf


// constant
let wscr = SystemParameters.PrimaryScreenWidth
let hscr = SystemParameters.PrimaryScreenHeight 


// text
let inline private replace __ str = (^a:(member set_Text:s->u) __, str)
let inline private append  __ str = (^a:(member Text:s) __) + str |> replace __ 

type [<Ex>] Ext () =                                                   
 [<Ex>]static member inline tf  (tbx,fmt) = kprintf (append tbx) fmt
 [<Ex>]static member inline tfn (tbx,fmt) = kprintf (_1 + "\r\n" >> append tbx) fmt


// panel
let inline wGridLength v ty __ = (^__:(member set_Width:_ -> u) __, GridLength(v, ty))
let inline wStar v __ = wGridLength (!.v) GridUnitType.Star __
let rows n (__:UniformGrid) = __.Rows    <- n
let cols n (__:UniformGrid) = __.Columns <- n