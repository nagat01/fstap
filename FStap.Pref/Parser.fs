﻿[<AutoOpen>]
module FStap.Toml
open System

let parseKey (__:Parser) = 
  __.parseChars(fun c -> Char.IsWhiteSpace.n c && c <> '=')

let parseString (__:Parser) = __ {
  do! Skip.char '"'
  let string = __.parseStringInner
  do! Skip.char '"'
  return string }

let parseValue (__:Parser) = __ {
  match __ with
  | Attempt parseString string -> return string
  | _ -> return! Parse.chars Char.IsWhiteSpace.n }

let parseKeyValue (line:s) =
  Parser line {
  do! Skip.ws 
  let! key = parseKey
  do! Skip.ws
  do! Skip.char '='
  do! Skip.ws
  let! value = parseValue
  return (key, value)
  }

