﻿[<AutoOpen>]
module FStap.Parser
open System

type op = CustomOperationAttribute

let inline isChar (line:s) (p:i) cond = p < line.Length && cond line.[p]
let inline bindB b f = if b then f () else None
let inline bindOpt m f = match m with Some __ -> f __ | _ -> None

type Parser(line:s) = 
  let line = line
  let mutable p = 0
  let subString pStart = 
    if pStart <> p 
    then Some line.[pStart .. p - 1] 
    else None
  
  member __._p with get() = p and set _p = p <- _p 
  member __.skipChar cond = 
    if isChar line p cond then p <- p + 1; true else false
  member __.skipChars cond = 
    while isChar line p cond do p <- p + 1
  member __.parseChar cond = 
    if __.skipChar cond then Some line.[p] else None
  member __.parseChars cond =
    let pStart = p
    __.skipChars cond
    subString pStart

  member __.parseStringInner =
    let pStart = p
    let f p f = 0 <= p && f line.[p] '\\'
    while p < line.Length && (line.[p] <> '"' || (f(p-1)(=) && f(p-2)(<>))) do p <- p + 1
    line.[pStart .. p - 1]

  member __.Bind(m:Parser -> u, f) = f (m __)
  member __.Bind(m:          b, f) = bindB m      f
  member __.Bind(m:Parser -> b, f) = bindB (m __) f
  member __.Bind(m:          'a opt, f) = bindOpt m      f
  member __.Bind(m:Parser -> 'a opt, f) = bindOpt (m __) f
  member __.Return m = Some m
  member __.ReturnFrom (m:          'a opt) = m
  member __.ReturnFrom (m:Parser -> 'a opt) = m __
  member __.Zero m = m

type Parse =
  static member inline chars cond (__:Parser) = __.parseChars cond

type Skip =
  static member inline char  cond = fun (__:Parser) -> __.skipChar cond
  static member inline char  c    = fun (__:Parser) -> __.skipChar(fun _c -> _c = c)
  static member inline chars cond (__:Parser) = __.skipChars cond
  static member inline ws         (__:Parser) = __.skipChars Char.IsWhiteSpace

let (|Attempt|_|) parser (__:Parser) =
  let pStart = __._p
  match parser __ with
  | Some result -> Some result
  | _ -> __._p <- pStart; None

