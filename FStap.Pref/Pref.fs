﻿[<AutoOpen>]
module FStap.Pref
open System.Collections.Generic

let inline get (dic:Dictionary<s, _>) parse key def = 
  let __ = key |> (dic.TryGetValue >> tupleToSome) >>= (parse >> tupleToSome) |? def |> sh
  __ => fun x -> dic.[key] <- string x
  __

let writeKeyValue (kv:KeyValuePair<s,s>) =
 sf "%s = \"%s\"" kv.Key ((kv.Value.rep @"\" @"\\").rep @"""" @"\""")

let readValue (value:s) = 
  (value.rep @"\""" @"""").rep @"\\" @"\"

type Pref(name:s, ?closed:u obs, ?isSave) = 
  let mutable _isSave = isSave |? true
  let _file = pwd +/ name + ".pref"
  let dic = Dictionary<s, _>()
  let _timer = mkTimer 30000
  let save() =
    if _isSave then
      seq { for kv in dic -> writeKeyValue kv }
      |> _file.writeLines 
  do
    if _file.fileOk then
      for line in _file.readLines do
        parseKeyValue line |%| fun (k, v) ->
          dic.[k] <- readValue v
    _timer =+ { save() } 
    closed |%| fun closed -> closed =+ { save() }

  member __.file = _file
  member __.isSave
    with set b = 
      _isSave <- b 
      if _isSave then _timer.Start() else _timer.Stop()

  member __.Item 
    with set key value = dic.[key] <- value 
    and  get key = dic.TryGetValue key |> tupleToSome

  member __.shS   key def = get dic (fun __ -> true, __) key def
  member __.shB   key def = get dic   b.TryParse key def
  member __.shI   key def = get dic   i.TryParse key def
  member __.shI64 key def = get dic i64.TryParse key def
  member __.shF   key def = get dic   f.TryParse key def

  member __.b   key = __.[key] >>= (  b.TryParse >> tupleToSome)
  member __.i   key = __.[key] >>= (  i.TryParse >> tupleToSome)
  member __.i64 key = __.[key] >>= (i64.TryParse >> tupleToSome)
  member __.f   key = __.[key] >>= (  f.TryParse >> tupleToSome)
  member __.ss  key def = ( __.[key] |? def ).split ";"
  member __.kvs key def = seq {
    for kv in ( __.[key] |? def ).split ";" do
      let kv = kv.split ","
      if kv.Length > 1 then
        yield (kv.[0], kv.[1]) }

  static member (?)   (__:Pref, key)       = __.[key]
  static member (?<-) (__:Pref, key, v:s)  = __.[key] <- v
  static member (?<-) (__:Pref, key, v:b)  = __.[key] <- string v
  static member (?<-) (__:Pref, key, v:i)  = __.[key] <- string v
  static member (?<-) (__:Pref, key, v:i64)= __.[key] <- string v
  static member (?<-) (__:Pref, key, v:f)  = __.[key] <- string v

 