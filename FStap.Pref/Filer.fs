﻿[<AutoOpen>]
module FStap.Filer
open System open System.Windows
open System.Windows.Media.Imaging
open System.IO
open Microsoft.Win32


type String with
  member __.fileStream mode f =
    use stream = new FileStream(__, mode)
    f stream
    stream.Close()
  member __.fileStreamOpen f = __.fileStream FileMode.Open f
  member __.fileStreamSave f = __.fileStream FileMode.Create f


type FileDialog with
  member __.show = __.ShowDialog().ok

type Forms.FolderBrowserDialog with
  member __.show = __.ShowDialog().HasFlag  Forms.DialogResult.OK


let mkFilter (exts:s) =
  exts.words |%> (fun ext -> sf "%sファイル(.%s)|*.%s" ext.upper ext ext) |> String.concat "|"

let mkFilterAllExt (exts:s) =
  sf "画像ファイル|%s" ((exts.words |%> "*." + _1).concat ";")

let selectDir (pref:Pref) key f =
  use __ = new Forms.FolderBrowserDialog()
  pref.[key] |%| __.set_SelectedPath
  if __.show then
    pref.[key] <- __.SelectedPath
    f __.SelectedPath

let private showDialog (pref:Pref) key (__:#FileDialog) filter f =
  __.Filter <- filter
  pref.[key] |%| __.set_InitialDirectory
  if __.show then
    f __
    pref.[key] <- __.FileName.dir

let saveFile pref key exts f = 
  showDialog pref key (SaveFileDialog()) (mkFilter exts) <| fun dlg ->
    let file = dlg.FileName
    if file.fileOk && file.isReadOnly 
    then mf "ファイル %s は読み取り専用なので、保存できません。" file 
    else f dlg.FileName

let openFile pref key exts f = 
  showDialog pref key (OpenFileDialog()) (mkFilter exts) <| fun dlg -> 
    f dlg.FileName

let openFiles pref key (exts:s) isDir f = 
  if isDir then 
    selectDir pref key <| fun dir -> 
      let files = exts.Split ' ' >>= fun ext -> dir.files ("*." + ext)
      if files.ok then f files
  else
    showDialog pref key (OpenFileDialog(Multiselect=true)) (mkFilter exts) <| fun dlg -> 
      f dlg.FileNames

let openFilesAllExt pref key (exts:s) isDir f = 
  if isDir then 
    selectDir pref key <| fun dir -> 
      let files = exts.Split ' ' >>= fun ext -> dir.files ("*." + ext)
      if files.ok then f files
  else
    showDialog pref key (OpenFileDialog(Multiselect=true)) (mkFilterAllExt exts) <| fun dlg -> 
      f dlg.FileNames

let openDir pref key exts f =  
  openFiles pref key exts true f

let saveImage pref bmpFrame = saveFile pref "folderSaveImage" "png" <| fun file ->
  use stream = new FileStream(file, FileMode.Create)
  let enc = PngBitmapEncoder()
  enc.Frames <+ bmpFrame
  enc.Save stream

let saveImageFe pref (fe:FE) =
  match feToBitmapFrame fe with
  | Some frame -> saveImage pref frame
  | None       -> msg "画像サイズが０以下なので保存できません。"

