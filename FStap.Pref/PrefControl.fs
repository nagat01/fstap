﻿[<AutoOpen>]
module FStap.PrefControl
open System.Windows open System.Windows.Controls


let restorePosSz (pref:Pref) (wnd:Window) =
  wnd.Loaded =+ {
    pref.f "wTop"    |%| wnd.set_Top
    pref.f "wLeft"   |%| wnd.set_Left
    pref.f "wWidth"  |%| wnd.set_Width
    pref.f "wHeight" |%| wnd.set_Height }
  wnd.Closing =+ {
    pref ? wTop    <- wnd.Top
    pref ? wLeft   <- wnd.Left
    pref ? wWidth  <- wnd.w
    pref ? wHeight <- wnd.h }


#if false
type PrefTextBox(pref:Pref, wnd:Window, key, def) as __ =
  inherit DockPanel()
  let _v = pref.shS key def
  let mkBtn (x:Image) = Button' (x $ w 12) $ bdr 1 "ccc" $ __.right
  let _       = Label' key $ fsz 10. $ w 120 $ __.left
  let btnSave = mkBtn Ico.save $ tip "設定項目を保存します"
  let btnLoad = mkBtn Ico.load $ tip "設定項目を読み込みます"
  let tbx = TextBox' _v.v $ wrap $ haStretch $ __
  do
    btnSave >< tbx.enter        =+ { _v <*-* tbx }
    btnLoad                     =+ { tbx <*-* _v }
    btnSave >< tbx >< tbx.enter =+ { ((tbx =. _v.v).If "000" "f44").fg tbx }
  member __.v 
    with get () = invoke wnd { V tbx.Text } 
    and  set s  = s $ _v |>* tbx
  member __.ss  = pref.ss  key def
  member __.kvs = pref.kvs key def
  member __.e   = btnSave >< tbx |%> fun _ -> tbx.Text


type PrefTextBoxLong(pref:Pref, wnd:Window, key, def) as __ =
  inherit DockPanel()
  let _v = pref.shS key def
  let btn (x:Image) s desc = 
    let btn = Button'() $ mgn2 2 0 $ tip desc
    let  sp = HStackPanel()  $ btn
    let   _ = x $ w 10 $ sp
    let   _ = Label' s $ sp $ fsz 10 
    btn

  let dpMenu   = DockPanel() $ lastChildFill $ __.top
  let  btnSave = btn Ico.save "save" "設定項目を保存します" $ dpMenu.right
  let  btnLoad = btn Ico.load "load" "設定項目を読み込みます" $ dpMenu.right
  let  _       = Label' key $ fsz 10 $ dpMenu.left
  let tbx      = TextBox' _v.v $ __.bottom $ multiline $ "eff".bg $ haStretch 
  do
    btnSave >< tbx.enter        =+ { _v <*-* tbx }
    btnLoad                     =+ { tbx <*-* _v }
    btnSave >< tbx >< tbx.enter =+ { ((tbx =. _v.v).If "000" "f44").fg tbx }
  member __.v 
    with get () = invoke wnd { V tbx.Text } 
    and  set s  = s $ _v |>* tbx
  member __.ss  = pref.ss  key def
  member __.kvs = pref.kvs key def
  member __.e   = btnSave >< tbx |%> fun _ -> tbx.Text 


type PrefPanelBase (pref:Pref, wnd:Window, pnl:Panel, e:u obs) as __ =
  inherit StackPanel()
  let btn = Button' "戻る" $ __
  do 
    e >< btn =+ { 
      if wnd =. box __ then box pnl else box __
      |>* wnd }
  member __.s key def = PrefTextBox (pref, wnd, key, def) $ __

let icoPref = Ico.pref
#endif