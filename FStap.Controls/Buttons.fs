﻿[<AutoOpen>]
module FStap.Buttons


// button
type Button' (?ttl:o, ?bg, ?hover, ?push) as __ =
  inherit Label' ()
  let bg    = bg    |? "0000"
  let hover = hover |? "dff"
  let push  = push  |? "fdd" 
  do 
    __ $ bg.bg $ pad2 2 1 $ vaStretch |> vcaCenter
    ttl |%|* __ 
  let f (co:s) _ = co.bg __
  member val e = __.lClick (f hover, f push, f bg)

type ShadowedButton (ttl, bg, ?ttlon) as __ =
  inherit Button' (ttl, bg= bg, hover= bg.co.dark(24).str)
  do
    ttlon |%| fun ttlon ->
      __.e =+ { __ <*- If (__.Content = ttlon) ttl ttlon }
  member __.v = __.Content <> ttl


type Button'' (ttl:o, ?ttlon, ?lead, ?trail, ?bg, ?hover, ?push, ?on, ?v) as __ =
  inherit HStackPanel()
  let mutable _v = v |? false
  let bg    = bg    |? "0000"
  let on    = on    |? bg
  let ttlon = ttlon |? ttl
  let hover = hover |? "dff"
  let push  = push  |? "fdd"
  do  lead  |%| fun _ -> Label' lead.v  |>* __
  let lbl = Label' ttl $ __ $ pad2 2 1
  do  trail |%| fun _ -> Label' trail.v |>* __
  let f (co:s) _ = co.bg __
  let set _ = 
    f (_v.If on bg) ()
    _v.If ttlon ttl |>* lbl
  do 
    __ |> vaStretch 
    set ()
  member __.v with get () = _v and set v = _v <- v; set ()
  member val e = __.lClick (f hover, f push, set)

