﻿[<AutoOpen>]
module FStap.Misc


let Ico = ImageProvider 24.

let inline (=*>*) m n = m => fun _ -> n <*-* m
let inline act    f m = m => f
let inline actv   f m = m => fun _ -> f <-* m
let inline depend n m = m =*>* n
let inline twoWay n m = m =*>* n; n =*>* m

let changeTheme _ = launch (@"C:\nagat01\themes".files("").rand)
