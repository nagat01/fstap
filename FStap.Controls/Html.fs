﻿[<AutoOpen>]
module FStap.Html
open System.Net
open FSharp.Data

let isUrlValid (url:s) = task' { yield tryd {
  let req = WebRequest.Create url :?> HttpWebRequest
  req.MediaType <- "HEAD"
  use res = req.GetResponse() :?> HttpWebResponse
  yield res.StatusCode = HttpStatusCode.OK } }

(*
// parse
type HtmlNode with
  member __.href  = (HtmlNode.attribute "href" __).Value()
  member __.href' = try_ { V __.href }
  member __.attr (name:s) = try_ { V (HtmlNode.attribute name __) }

let getHtml (url:s) : s a = task' {
  yield Http.RequestString url }

//let getBrowserHtml (webb:WebBrowser) = 
//  let a = (webb.Document :?> mshtml.IHTMLDocument2)
//  a.body.outerHTML

let getHtmlTitle (html:s) = html.mat(@"<title>\s*(.+?)\s*</title>").group(1).htmlDecode
let getTitle url = async {
  let! b = isUrlValid url 
  if b 
  then return! getHtml url |%> getHtmlTitle |%> Some 
  else return None }
*)

