﻿module FStap.WebBrowser
open System 
open System.Windows.Controls
open System.Reflection        
open System.Runtime.InteropServices


[<Guid("b722bccb-4e68-101b-a2bc-00aa00404770"); ComImport; InterfaceType(ComInterfaceType.InterfaceIsIUnknown)>]
type IOleCommandTarget =
  abstract member QueryStatus: u
  abstract member Exec: Guid byref * u32 * u32 * o byref * o byref -> u

[<Guid "6d5140c1-7436-11ce-8034-00aa006009fa"; ComImport; InterfaceType(ComInterfaceType.InterfaceIsIUnknown)>]   
type private IServiceProvider = 
  abstract member QueryService: Guid byref * Guid byref -> [<MarshalAs(UnmanagedType.IUnknown)>] o

[<Dll "urlmon.dll"; PreserveSig>] extern [<MarshalAs(UnmanagedType.Error)>] 
  i CoInternetSetFeatureEnabled (i FeatureEntry, [<MarshalAs(UnmanagedType.U4)>] i flags, b enable)
// 21: FEATURE_DISABLE_NAVIGATION_SOUNDS  2; SET_FEATURE_ON_PROCESS
let silentWebb () = CoInternetSetFeatureEnabled(21, 2, true).ig


type WebBrowser with 
  member __.navigate wnd (url:s) = task' { invoke wnd { __.Navigate url } }
  member __.openFind wnd = task' { invoke wnd { try' { 
    let mutable o = o()
    let mutable guid = Guid "ED016940-BD5B-11CF-BA4E-00C04FD70816"
    (__.Document :?> IOleCommandTarget).Exec(&guid, 1u, 0u, &o, &o) } } }
  member __.hideScriptErrors = iopt {
    let! fi = typeof<WebBrowser>.GetField("_axIWebBrowser2", BindingFlags.Instance ||| BindingFlags.NonPublic)
    let! o = fi.GetValue __
    o.GetType().InvokeMember("Silent", BindingFlags.SetProperty, null, o, [| true |]).ig }
  // member __.noPopup =
  //  let mutable sid = Guid "0002DF05-0000-0000-C000-000000000046"
  //  let mutable iid = typeof<SHDocVw.IWebBrowser2>.GUID
  //  let browser = (__.Document :?> IServiceProvider).QueryService (&sid, &iid)
  //  SHDocVw.DWebBrowserEvents2_NewWindow3EventHandler(fun _ cancel _ _ _ -> cancel <- true)
  //  |> (browser :?> SHDocVw.DWebBrowserEvents2_Event).add_NewWindow3

