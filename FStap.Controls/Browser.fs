﻿[<AutoOpen>]
module FStap.Browser
open System.Diagnostics
open System.IO
open System.Windows.Automation

type Browser = 
  Chrome | Firefox | IE
  override __.ToString() =
    match __ with 
    | Chrome  -> "chrome" 
    | Firefox -> "firefox" 
    | IE      -> "iexplore"

let browserOf = function
  | "chrome"   -> Some Chrome
  | "firefox"  -> Some Firefox
  | "iexplore" -> Some IE
  | _          -> None

open Microsoft.Win32
let defaultBrowser () =
  Registry.ClassesRoot.OpenSubKey(@"HTTP\shell\open\command", false).GetValue(null)
    .s.lower.rep (char 34).s "" |> Path.GetFileName 
    |> browserOf |> function 
      | Some browser -> browser 
      | _            -> failwith "not supported browser!" 


// top url
let topUrlChrome () = 
  let res = ref None
  for p in Process.GetProcessesByName "chrome" do try' {
    if p.MainWindowHandle <> 0n then
      let elem = AutomationElement.FromHandle p.MainWindowHandle
      let elem = elem.FindFirst(TreeScope.Descendants, PropertyCondition(AutomationElement.NameProperty, "Address and search bar"))
      if elem <> null then
        let pats = elem.GetSupportedPatterns()
        if pats.Length > 0 then
          let v = elem.GetCurrentPattern pats.[0] :?> ValuePattern 
          res := Some v.Current.Value }
  !res |%> fun url -> if url.StartsWith "http" then url else "http://" + url

let topUrlFirefox () = None

let topUrlIE pat = None
// try_ { 
// let h = GetForegroundWindow()
//  yield Seq.last <| seq {
//  for ie:SHDocVw.InternetExplorer in SHDocVw.ShellWindowsClass() |> cast do
//    let url = ie.LocationURL
//    match pat with
//    | "" -> if h = ni ie.HWND then yield url
//    | _  -> if url.has pat    then yield url } }

let topUrl patForIE = function
  | Chrome  -> topUrlChrome()
  | Firefox -> topUrlFirefox()
  | IE      -> topUrlIE patForIE


type private BrowserItem (ttl, browser:Browser) as __ =
  inherit HStackPanel()
  do __ |> cs [Label' ttl]
  member __.v = browser

type BrowserSelector () as __ =
  inherit ComboBox'()
  let items = 
    ["自動選択", defaultBrowser(); "chrome", Chrome; "firefox", Firefox; "ie", IE] 
    |%> fun (s, browser) -> BrowserItem (s, browser)
  do __ $ itemsi 0 items |> vcaCenter 
  member __.v = (__.item :?> BrowserItem).v
  member __.openLink isNewWindow link = async { 
    trym "ブラウザの起動に失敗しました。" {
      match __.v with
      | Chrome  -> "chrome",   link + If isNewWindow " --new-window" ""
      | Firefox -> "firefox",  If isNewWindow "-new-window " "" + link 
      | IE      -> "iexplore", link 
      |> Process.Start |> ig } }
  member __.newTab link = __.openLink false link
  member __.newWnd link = __.openLink true  link

  member __.topUrl filterForIE = topUrl filterForIE __.v

