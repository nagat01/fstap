﻿[<AutoOpen>]
module FStap.Controls          
open System open System.Windows open System.Windows.Controls
open System.Windows.Input
open System.Windows.Controls.Primitives
open System.Windows.Shapes
open System.Windows.Media


// selector
type Nud< 't when 't: equality> (_is: 't list, ?lead, ?trail, ?w) as __ =
  inherit DockPanel()
  let mutable _idx = 0
  let _e = Event<_>()

  let sel d _ = __.idx <- within 0 (_is.len - 1) (_idx + d)
  let add x   = x |%| fun x -> Label' x |> __.add
  let btn f b = 
    let tri = 
      Polygon() $ pnt3 (po 6 f) (po 0 b) (po 12 b)
      $ stroke 1 "77f" $ fill "ccf" $ rounded
    Button' tri $ pad2 2 1 $ vcaCenter $ vaStretch

  do add lead 
  let _lbl = Label' _is.[0] $ bold $ "f77".fg $ hcaRight $ __.left
  do w |%| _lbl.W
  do add trail 
  let ugrid = UniformGrid' (r=2) $ __.right
  let up    = btn 0 5 $ ugrid $ act (sel 1)
  let down  = btn 5 0 $ ugrid $ act (sel -1)

  member __.lbl = _lbl
  member __.e = _e.Publish
  member __.idx 
    with get () = _idx
    and  set i  = __.v <- _is.[i] 
  member __.v 
    with get () = _is.[_idx]
    and  set v  = 
      _lbl <*- (v :> o)
      _idx <- _is.idx ((=)v)
      _e.Trigger _idx


// auto complete
type TextBox with
  member __.autoComplete (cands:u->#seq<s>, ?f, ?completeSingleCand) =
    let mutable isInput = true
    let completeSingleCand = completeSingleCand |? true
    let lbx = ListBox()  
    let pop = Popup (Child= lbx, AllowsTransparency= true, PlacementTarget= __, Placement= PlacementMode.Bottom)
    let show () = 
      lbx.clear
      lbx |> itemsi 0 (cands())
      pop.IsOpen <- lbx.cnt > 0
    let setText text = 
      isInput <- false
      __.Text <- text
      __.Select (0, __.Text.len)
      pop.IsOpen <- false
      __.MoveFocus(TraversalRequest FocusNavigationDirection.Next).ig
    __ -? { V isInput.n } =+ { 
      isInput <- true
      match cands().list >%? fun cand -> cand.lower.StartsWith __.Text.lower  with
      | [cand] when completeSingleCand -> setText cand
      | cand::_ -> show(); lbx.item <- cand
      | _       -> show() }
    __.pkd Key.Up    =+ { lbx.selPrev }
    __.pkd Key.Down  =+ { lbx.selNext }
    __.pkd Key.Tab   =+ { setText (lbx.item :?> s) }
    __.pkd Key.Enter >< __.pkd Key.Tab => fun x -> f |%| (fun f -> f x)
    __.GotFocus  =+ { show() }
    __.LostFocus >< __.IsVisibleChanged -? { Not __.vis } =+ { pop.IsOpen <- false }


type GuideBar() as __ =
  inherit TextBlock()
  do 
    __ $ "faa".fg $ "fff".bg |> bold
    __.hover =+ { if __.defDisc.good then __ <*- __.defDisc }
    __.Loaded =+ { mw.mleave =+ { __ <*- "" } }
  member __.v with set s = __ <*- __.prefix + s
  member val prefix = "説明: " with get,set
  member val defDisc:s = "操作ガイド: ここに操作ガイドを表示します。" with get,set
let guide = GuideBar()
let tip (s:s)     (__:UIE) = __.hover =+ { guide <*- s }
let tip' (f:u->s) (__:UIE) = __.hover =+ { guide <*- f() }


// slider
(*
type TitledSlider (ttl, mi, ma, fg:s) as __ =
  inherit StackPanel()
  let lbl = Label'() $ fg.co.fg $ fsz 10 $ equalWidth $ haCenter $ __
  let sli = Slider' (mi, ma) $ haStretch $ __
  let set (v:f) = lbl <*- sf "%s% 4d" ttl v.ri
  do
    sli.LayoutTransform <- ScaleTransform (ScaleY= 0.7)
    set sli.v
    sli =+ { set sli.v }
  member __.v with get () = sli.v and set v = sli.v <- v
  member __.e = ( sli.Loaded >< sli.ValueChanged ) -% { V sli.v }
  new (ttl, mi, ma) as __ = TitledSlider(ttl, mi, ma, "000") then __ |> "fff".bg
  new (ttl, key, mi, ma, d) as __ = TitledSlider(ttl, mi, ma) then __ |> preff' key d 


type FloatVolume(ttl, ma, ?def, ?trail:s) as __ =
  inherit HStackPanel()
  let def = pref.f ttl |? (def |? 0.)
  let _   = Lbl ttl              $ __
  let lbl = Lbl ()               $ __
  do trail |%| fun trail -> Lbl trail |>* __
  let sli = Slider' (0., ma, w = 50.) $ __
  do
    psave ttl sli
    sli >< loaded =+ { lbl <*- sf "%.2f" sli.v }
    sli <*- def
  member __.v = sli.v
  member __.e = sli.ValueChanged


type IntVolume(ttl, ma:i, ?def, ?trail:s) as __ =
  inherit HStackPanel()
  let def = pref.i ttl |? (def |? 0)
  let _   = Lbl ttl                $ __
  let lbl = Lbl ()                 $ __
  do trail |%| fun trail -> Lbl trail |>* __
  let sli = Slider'(0., ma.f, w = 50.) $ __
  do
    psave ttl sli
    sli >< loaded =+ { lbl <*- sf "%.0f" sli.v }
    sli <*- def
  member __.v = sli.v.i
  member __.e = sli.ValueChanged


type VolumePanel (ico, ?co) as __ =
  inherit HStackPanel ()
  let _   = Label' ico $ __
  let sli = Slider'(0., 1., w=50.) $ vaCenter $ __
  do 
    __ $ (co |? "cfc").bg |> vaStretch
  member __.v with get() = sli.v and set v = sli.v <- v
  member __.vexp = sli.vexp
  member __.e = sli -% { V sli.Value }


type SpeechVolumePanel (v) as __ =
  inherit VolumePanel (Ico.volume)
  do __ 
    $ ploadf' "speechVolume" v
    $ psave "speechVolume"
    |> tip "読み上げボリューム: 読み上げのボリュームを変えられます。"
*)

// title bar
type TitleBar (wnd:Window, child:FE) as __ =
  inherit Popup (Child= child, IsOpen= true)
  let mutable maw, mih = 0., 200.
  do
    wnd.Loaded >< wnd.LocationChanged >< child.LayoutUpdated  =+ {
      maw <- [maw; child.w; child.aw].filter((<>)nan) |> Seq.max
      __.HorizontalOffset <- wnd.Left + maw + 26.
      __.VerticalOffset   <- wnd.Top + 3.5
      if wnd.Left < 0. then
        __.W <| max 0. (maw + wnd.Left)
        mih <- min mih child.ah
        __.H mih
      else
        __.W <| max __.w maw }
    wnd.StateChanged >=< wnd.Activated >=< wnd.Deactivated =+ {
      __.IsOpen <- wnd.IsActive && wnd.WindowState <> WindowState.Minimized }

// transformer
type TrOrd = TRS | TSR | RTS | RST | STR | SRT
type Transformer (tg:UIE, ord:TrOrd, ?scCenter, ?rotCenter) as __ =
  let g   = TransformGroup     ()
  let tr  = TranslateTransform () 
  let rot = RotateTransform    () 
  let sc  = ScaleTransform     ()
  let setCenter () =
    let getCenter center = (center |? fun _ -> tg.rsz.po /. 2.)()
    sc.center  (getCenter scCenter)
    rot.center (getCenter rotCenter)
  let init () =
    sc.Scale  <- 1., 1.
    rot.Angle <- 0.
    tr.X <- 0.     
    tr.Y <- 0.
  do 
    tg.RenderTransform <- g          
    match ord with 
    | TRS -> [tr;rot;sc] : Transform list 
    | TSR -> [tr;sc;rot] 
    | RTS -> [rot;tr;sc] 
    | RST -> [rot;sc;tr] 
    | STR -> [sc;tr;rot] 
    | SRT -> [sc;rot;tr]
    |%| g.add
    init ()

  member __.Move (p:Po) = 
    setCenter()
    tr.X <- p.X
    tr.Y <- p.Y
  member __.Move vec = __.Move (po tr.X tr.Y + vec)
  member __.Scale v  = setCenter();  sc.Scale <- v, v
  member __.Zoom v   = 
    let mi = match min tg.rw tg.rh with 0. -> 0.001 | d -> 1. / d
    let ma = 100.
    let sc = within mi ma (sc.ScaleX * (exp v))
    __.Scale sc
  member __.Rotate v = setCenter();  rot.Angle <- rot.Angle + v
  member __.Angle  v = setCenter();  rot.Angle <- v
  member __.Init  = init ()  
  member __.trX   = tr.X
  member __.trY   = tr.Y
  member __.scX   = sc.ScaleX
  member __.scY   = sc.ScaleY
  member __.sc'   = sc.ScaleX
  member __.angle = rot.Angle
  member __.group = g
  member __.e     = sc.Changed >< tr.Changed >< rot.Changed

type MouseTransformer (tg:UIE) as __ =
  inherit Transformer (tg, RST)
  do
    tg.lDragAbs => fun {Vec=v} -> 
      vec (__.isFliped.If -v.X v.X) v.Y |> __.Move
    tg.rDragAbs => fun {Vec=v} -> 
      __.isFliped.If -v.X v.X |> __.Rotate 
      v.Y * -0.01 |> __.Zoom
  member val isFliped:b = false with get,set
