﻿#r "PresentationCore"
#r "PresentationFramework"
#r "WindowsBase"
#r "System.Xaml"
#r "System.Speech"
#r @"..\..\FStap\FStap.Core\bin\Debug\FStap.Core.dll"
#r @"..\..\FStap\FStap.System\bin\Debug\FStap.System.dll"
#r @"..\..\FStap\FStap.WPF\bin\Debug\FStap.WPF.dll"
#r @"..\..\FStap\FStap.Controls\bin\Debug\FStap.Controls.dll"
open FStap
open System
open System.Collections.Generic
open System.Windows
open System.Windows.Controls
open System.Windows.Shapes


// nest seq
for y in 0..9 do 
  for x in 0..9 do 
    ()

for x,y in cart2 [0..9] [0..9] do
  ()


// safe head
try Some (List.head [0..9]) with _ -> None
tryHead [0..9] 

// safe max
try Some (List.max [0..9]) with _ -> None
tryMax [0..9]


// all
[true; true] |> List.forall id
[true; true] |> all

// none
[false; false] |> List.exists id |> not
[false; false] |> none

// any
[true; false] |> List.exists id
[true; false] |> any


// sort
["lisp"; "ml"; "algol"] |> List.sortBy String.length
["lisp"; "ml"; "algol"].sort length

// find
[1; 5; 10] |> List.find (fun x -> x > 5)
[1; 5; 10].find (_' (>) 5)

// index of
"abcde" |> Seq.findIndex((=)'c')
"abcde".indexOf

// has element
[ 1..9 ] |> List.exists ((=)5)
[ 1..9 ].have 5

// max
[ "lisp"; "ml"; "algol" ] |> List.maxBy String.length
[ "lisp"; "ml"; "algol" ].max length

// min
[ "lisp"; "ml"; "algol" ] |> List.minBy String.length
[ "lisp"; "ml"; "algol" ].min length

// sum by
[ 1..9 ] |> List.sumBy (pown 2)
[ 1..9 ].sumBy (pown 2)

// average by
[ 1. .. 9. ] |> List.averageBy log
[ 1. .. 9. ].aveBy log

// remove
[| 1..9 |] |> Array.filter((<>)5)
[| 1..9 |].rem 5