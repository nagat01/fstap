﻿#r "PresentationCore"
#r "PresentationFramework"
#r "WindowsBase"
#r "System.Xaml"
#r "System.Speech"
#r @"..\..\FStap\FStap.Core\bin\Debug\FStap.Core.dll"
#r @"..\..\FStap\FStap.System\bin\Debug\FStap.System.dll"
#r @"..\..\FStap\FStap.WPF\bin\Debug\FStap.WPF.dll"
#r @"..\..\FStap\FStap.Controls\bin\Debug\FStap.Controls.dll"
open FStap
open System
open System.Collections.Generic
open System.Windows
open System.Windows.Controls
open System.Windows.Shapes


// to seq
[ 0..9 ] |> List.toSeq
[ 0..9 ] |> toSeq

[| 0..9 |] |> Array.toSeq
[| 0..9 |] |> toSeq


// to list
Some "a" |> Option.toList
Some "a" |> toList

Seq.singleton "a" |> List.ofSeq
Seq.singleton "a" |> toList

[|"a"|] |> List.ofArray
[|"a"|] |> toList


// reverse collection
[1.; 2.] |> List.rev
[1.; 2.] |> rev

[|1.; 2.|] |> Array.rev
[|1.; 2.|] |> rev


// mapi
seq { 0..9 } |> Seq.mapi(fun i x -> i * x)
seq { 0..9 } |> mapi(fun i x -> i * x)

[ 0..9 ] |> List.mapi(fun i x -> i * x)
[ 0..9 ] |> mapi(fun i x -> i * x)

[| 0..9 |] |> Array.mapi(fun i x -> i * x)
[| 0..9 |] |> mapi(fun i x -> i * x)


// partition
[ 0..9 ] |> List.partition(fun x -> x < 5)
[ 0..9 ] |> partition(fun x -> x < 5)

[| 0..9 |] |> Array.partition(fun x -> x < 5)
[| 0..9 |] |> partition(fun x -> x < 5)

seq { 0..9 } 
|> Seq.toList |> List.partition(fun x -> x < 5) 
|> fun(part1, part2) -> Seq.ofList part1, Seq.ofList part2
seq { 0..9 } |> partition(fun x -> x < 5)


// concat
seq { for i in 0..9 -> seq { 0..9 } } |> Seq.concat
seq { for i in 0..9 -> seq { 0..9 } } |> concat'

[Button().Click :?> _ IObservable] |> Seq.reduce Observable.merge
[Button().Click :?> _ IObservable] |> concat'

