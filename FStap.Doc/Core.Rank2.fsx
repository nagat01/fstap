﻿#r "PresentationCore"
#r "PresentationFramework"
#r "WindowsBase"
#r "System.Xaml"
#r "System.Speech"
#r @"..\..\FStap\FStap.Core\bin\Debug\FStap.Core.dll"
#r @"..\..\FStap\FStap.System\bin\Debug\FStap.System.dll"
#r @"..\..\FStap\FStap.WPF\bin\Debug\FStap.WPF.dll"
#r @"..\..\FStap\FStap.Controls\bin\Debug\FStap.Controls.dll"
open FStap
open System
open System.Collections.Generic
open System.Windows
open System.Windows.Controls
open System.Windows.Shapes


// smaller
(sh 10).v < (sh 5).v
sh 10 <. sh 5

// greater
(sh 10).v > (sh 5).v
sh 10 >. sh 5

// equal
(sh 10).v = (sh 5).v
sh 10 =. sh 5

// not equal
(sh 10).v <> (sh 5).v
sh 10 <>. sh 5

// or
defaultArg None 10
None |? 10

defaultArg None 10, defaultArg None 10
(None, None) ||? (10, 10)

null |> fun x -> if x <> null then x else Object()
null |?? Object()

// binary operation with option value
(10, Some 10) |> function (a, Some b) -> Some (a + b) | _ -> None
Some 10 +? 10

// to option
Double.TryParse "10." |> fun (b, v) -> if b then Some v else None
Double.TryParse "10." |> toOpt

Nullable 10 |> fun m -> if m.HasValue then Some m.Value else None
Nullable 10 |> toOpt

// place holder
10 |> (fun x -> x * 2) |> (fun x -> 100 / x) |> (fun x -> x + 1)
10 |> _1 * 2 |> 100 / _1 |> _1 + 1

// primary value
Slider() |*> printf "%f"

// assign
Label().Content <- "it's label"
Label() <*- "it's label"

Label() |> fun x -> StackPanel().Children.Add x |> ignore
Label() |> fun x -> StackPanel().add x
Label() |>* StackPanel().add
Label() |>* StackPanel()

Label() |> fun x -> StackPanel().Children.Add x |> ignore; x
Label() |> fun x -> StackPanel().add x; x
Label() $ StackPanel().add
Label() $ StackPanel()


// primary event
Observable.merge (Button().Click) (Button().Click) |> Observable.add (fun e -> ())
Button().Click >=< Button().Click => fun e -> ()