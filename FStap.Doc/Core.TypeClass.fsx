﻿#r "PresentationCore"
#r "PresentationFramework"
#r "WindowsBase"
#r "System.Xaml"
#r "System.Speech"
#r @"..\..\FStap\FStap.Core\bin\Debug\FStap.Core.dll"
#r @"..\..\FStap\FStap.System\bin\Debug\FStap.System.dll"
#r @"..\..\FStap\FStap.WPF\bin\Debug\FStap.WPF.dll"
#r @"..\..\FStap\FStap.Controls\bin\Debug\FStap.Controls.dll"
open FStap
open System
open System.Collections.Generic
open System.Windows
open System.Windows.Controls
open System.Windows.Shapes

// collect collection
seq { 0..9 } |> Seq.collect Seq.singleton
seq { 0..9 } |> collect Seq.singleton
seq { 0..9 } >>= Seq.singleton

[ 0..9 ] |> List.collect (fun x -> [x])
[ 0..9 ] |> collect (fun x -> [x])
[ 0..9 ] >>= (fun x -> [x])


// map collection
seq { 0..9 } |> Seq.map (pown 2)
seq { 0..9 } |> map (pown 2)
seq { 0..9 } |%> (pown 2)

Button().Click |> Observable.map string
Button().Click |> map string
Button().Click |%> string


// iter collection
[ 0..9 ] |> List.iter (printf "%d")
[ 0..9 ] |> iter (printf "%d")
[ 0..9 ] |%| printf "%d"

None |> Option.iter (printf "%A")
None |> iter (printf "%A")
None |%| printf "%A"


// merge
Observable.merge (Button().Click) (Button().Click)
merge (Button().Click) (Button().Click)
Button().Click ++ Button().Click

Array.append [||] [||]
merge [||] [||]
[||] ++ [||]

Seq.append (seq {1..9}) (Seq.singleton 10)
seq {1..9} +++ 10

Array.append [|"a"|] [|"b"; "c"|]
"a" +++ [|"b"; "c"|]


// concat
List.concat [[]]
mconcat [[]]


// to tuple
[10; 20] |> function a :: b :: _ -> a, b | _ -> failwith ""
[10; 20] |> tuple2

[|"a", "b"|] |> fun xs -> xs.[0], xs.[1]
[|"a", "b"|] |> tuple2