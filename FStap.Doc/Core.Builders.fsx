﻿#r "PresentationCore"
#r "PresentationFramework"
#r "WindowsBase"
#r "System.Xaml"
#r "System.Speech"
#r @"..\..\FStap\FStap.Core\bin\Debug\FStap.Core.dll"
#r @"..\..\FStap\FStap.System\bin\Debug\FStap.System.dll"
#r @"..\..\FStap\FStap.WPF\bin\Debug\FStap.WPF.dll"
#r @"..\..\FStap\FStap.Controls\bin\Debug\FStap.Controls.dll"
open FStap
open System
open System.Collections.Generic
open System.Threading
open System.Threading.Tasks
open System.Windows
open System.Windows.Controls
open System.Windows.Shapes

// unit -> unit function
fun _ -> printfn "hello"
uf { printfn "hello" }


// array init
Array.init 10 (fun _ -> Button())
array' 10 { V (Button()) }

// array 2d init
Array2D.init 10 10 (fun _ _ -> Button())
array2' 10 10 { V (Button()) }

// try
try failwith "" with _ -> () 
try' { failwith "" }

try List.find((=)true) [] with _ -> false
tryb { V (List.find((=)true) []) }

try Double.Parse "10." |> Some with _ -> None
try_ { V (Double.Parse "10.") }

// option
match Some 10, Some 10 with 
| Some x, Some y -> 
  Some(x + y) 
| _ -> 
  None 
opt { 
let! x = Some 10 
let! y = Some 10 
return x + y }

// async
async {
do! Async.Sleep 10
do! Async.Sleep (0.1 * 1000. |> int)
let! a = Button().Click |> awaitObs
let! b = new Task<unit>(fun _ -> ()) |> Async.AwaitTask
let! is = async { return seq { 0..9 } }
for i in is do
  do! Async.Sleep i
}
async {
do! 10
do! 0.1
let! a = Button().Click 
let! b = new Task<unit>(fun _ -> ())
for i in Async.ret (seq { 0..9 }) do
  do! Async.Sleep i }

async { () } |> Async.Start
start' { () } 

async { () } |> Async.StartImmediate
immediate' { () } 

( try async { failwith "" } with _ -> async { () } ) |> Async.StartImmediate
tryImmediate' { failwith "" } 

// task
async { Thread.Sleep 100 } |> Async.StartAsTask |> Async.AwaitTask
task' { Thread.Sleep 100 }

// times
10 .times { printf "!" }
for _ in 1..10 do printf "!"


// handler
Button().Click.Add <| fun _ -> Async.Sleep 100 |> Async.StartImmediate
Button() =+! { do! Async.Sleep 100 }

Slider().ValueChanged.Add <| fun _ -> printf "changed!"
Slider() =+ { printf "changed!" }

Window().SizeChanged.Add <| fun _ -> try failwith "" with _ -> ()
Window().SizeChanged =+? { failwith "" }


// event
Button().Click |> Event.map(fun _ -> DateTime.Now)
Button() -% { V DateTime.Now }

Slider().ValueChanged |> Event.filter(fun e -> DateTime.Now.Second % 2 = 0)
Slider() -? { V (DateTime.Now.Second % 2 = 0) }