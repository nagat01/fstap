﻿#r "PresentationCore"
#r "PresentationFramework"
#r "WindowsBase"
#r "System.Xaml"
#r "System.Speech"
#r @"..\..\FStap\FStap.Core\bin\Debug\FStap.Core.dll"
#r @"..\..\FStap\FStap.System\bin\Debug\FStap.System.dll"
#r @"..\..\FStap\FStap.WPF\bin\Debug\FStap.WPF.dll"
#r @"..\..\FStap\FStap.Controls\bin\Debug\FStap.Controls.dll"
open FStap
open System
open System.Collections.Generic
open System.Windows
open System.Windows.Controls
open System.Windows.Shapes


// option
Some 4 |> Option.bind (fun x -> if x > 5 then Some x else None)
Some 4 |> Option.filter (fun x -> x > 5) 


// seq
async { for i in 0..9 do do! Async.Sleep i }
Seq.iterAsync Async.Sleep

["a"; ""] |> Seq.mapi(fun i x -> x.Length > i, x) |> Seq.choose(fun (b, x) -> if b then Some x else None)
["a"; ""] |> Seq.filteri(fun i x -> x.Length > i)


// async
fun (m:_ Async) -> async { let! x = m in return! Async.Sleep x }
Async.bind Async.Sleep

async { let! x = async { return "polly" } in return String.length x }
Async.ret "polly" |> Async.map String.length


// observable
[for _ in 0..9 -> Button().Click :> _ IObservable] |> Seq.reduce Observable.merge
[for _ in 0..9 -> Button().Click :> _ IObservable] |> Observable.concat


// option
(Some "lithium") |> Option.iter (printf "%s")
(Some "lithium").iter (printf "%s")

(Some 10) |> Option.bind(fun x -> if x > 5 then Some x else None)
(Some 10).filter(fun x -> x > 5)

match None with Some x -> [x] | _ -> []
None.list

defaultArg None 1.
None.Or 1.


// func
String.IsNullOrEmpty "" |> not
String.IsNullOrEmpty.n ""