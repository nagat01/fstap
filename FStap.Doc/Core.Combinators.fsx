﻿#r "PresentationCore"
#r "PresentationFramework"
#r "WindowsBase"
#r "System.Xaml"
#r "System.Speech"
#r @"..\..\FStap\FStap.Core\bin\Debug\FStap.Core.dll"
#r @"..\..\FStap\FStap.System\bin\Debug\FStap.System.dll"
#r @"..\..\FStap\FStap.WPF\bin\Debug\FStap.WPF.dll"
#r @"..\..\FStap\FStap.Controls\bin\Debug\FStap.Controls.dll"
open FStap
open System
open System.Collections.Generic
open System.Windows
open System.Windows.Controls
open System.Windows.Shapes


// add operator
DockPanel().Children.Add <| Button()
DockPanel().Children <+ Button()

"item1" |> ListBox().Items.Add
"item1" >+ ListBox().Items

Option.iter (ResizeArray().Add) (Some 10)
ResizeArray() <+? Some 10

[Point(10., 10.); Point(20., 20.)] |> List.iter (Polygon().Points.Add)
[Point(10., 10.); Point(20., 20.)] >%+ Polygon().Points


// if function
if 3.15 < Math.PI then "yes" else "no"
If (3.15 < Math.PI) "yes" "no"


// bool to Option
if String.IsNullOrWhiteSpace "" then None else Some ""
bToOpt (String.IsNullOrWhiteSpace "") ""

Double.TryParse "100." |> fun (success,value) -> if success then Some value else None
Double.TryParse "100." |> tupleToSome


// values
let _ : TimeSpan = Unchecked.defaultof<_>
let _ : TimeSpan = Zero


// detuple
(10, "a", Object()) |> fun (_,x,_) -> x
(10, "a", Object()) |> _x_ 

// modify tuple
(1., 2.) |> fun (a,b) -> a, sqrt b
(1., 2.) |> _f sqrt


// mutable operations
do
  let mutable i = 0
  while i < 10 do 
    i <- i + 1
  i <- i * 10
do
  let mutable i = 0 
  while incM &i < 10 do 
    ()
  &i *= 10


// async
async {
while true do
  do! async { [ for i in 0. .. 10000. -> () ] |> ignore } |> Async.StartAsTask
  do! Async.Sleep 10 }
|> Async.StartImmediate

aLoop 10 (task(fun _ -> [ for i in 0. .. 10000. -> () ] |> ignore)) |> immediate

